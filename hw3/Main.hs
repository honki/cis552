-- Advanced Programming, HW 3
-- by  Sudarshan Muralidhar (smural)
-- and Hong Kim (honki)

{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults -fno-warn-orphans #-}
{-# LANGUAGE FlexibleInstances #-}


module Main where

import Data.List
import Data.Maybe

-- These two import declarations say: (1) import the type Map from the
-- module Data.Map as an unqualified identifier, so that we can use
-- just `Map a b` as a type; and (2) import everything else from
-- Data.Map as qualified identifiers, written Map.member, Map.!, etc.
import Data.Map (Map)
import qualified Data.Map as Map

import Control.Monad

import Test.QuickCheck
import System.Random



------------------------------------------------------------------------------
-- Problem 1: QuickCheck properties for lists

prop_const' :: Eq a => a -> a -> Bool
prop_const' a b = const a b == a

-- *Main> quickCheck (prop_const :: Char -> Char -> Bool)

data Undefined
instance Testable Undefined where
  property = error "Unimplemented property"

prop_const :: Eq a => (a -> a -> a) -> a -> a -> Bool
prop_const const' a b = const' a b == a

const_bug :: a -> a -> a
const_bug _ b = b -- Oops: this returns the *second* argument, not the first.

prop_minimum :: Ord a => ([a] -> a) -> [a] -> Bool
prop_minimum minimum' l = all (m <=) l
                          where m = minimum' l

minimum_bug :: Ord a => [a] -> a
minimum_bug = maximum

newtype SmallNonNeg a = SmallNonNeg a deriving (Eq, Ord, Show, Read)

instance (Num a, Random a, Arbitrary a) => Arbitrary (SmallNonNeg a) where
    arbitrary = liftM SmallNonNeg $ choose (0, m)
                where m = 1000
    shrink (SmallNonNeg x)= fmap SmallNonNeg $ shrink x

prop_replicate :: Eq a => (Int -> a -> [a]) -> (SmallNonNeg Int) -> a -> Bool
prop_replicate replicate' (SmallNonNeg n) x = all (x ==) res && n == length res
                                              where res = replicate' n x

replicate_bug :: Int -> a -> [a]
replicate_bug i n = replicate (i*2) n

prop_group_1 :: Eq a => ([a] -> [[a]]) -> [a] -> Bool
prop_group_1 group' l = concat res == l
                        where res = group' l

prop_group_2 :: Eq a => ([a] -> [[a]]) -> [a] -> Bool
prop_group_2 group' l = all (\x -> not $ null x && all (head x ==) x) res
                        where res = group' l

group_bug :: Eq a => [a] -> [[a]]
group_bug [] = [[]]
group_bug (x:xs) = [x] : (x:ys) : group_bug zs
                    where (ys,zs) = span (x ==) xs

-- Property 1: the length of the list should be unchanged
prop_reverse_1 :: ([a] -> [a]) -> [a] -> Bool
prop_reverse_1 reverse' l = length l == length (reverse' l)

-- Property 2: the element at the ith spot in the original list should be the element
-- at the (length - i - 1)th spot in the reversed list
prop_reverse_2 :: Eq a => ([a] -> [a]) -> [a] -> Bool
prop_reverse_2 reverse' l = all id $ is_reversed l (reverse' l)
                            where
                              is_reversed l' l2' =
                                foldr (\x r -> (x == l2' !! length r):r) [] l'

reverse_bug_1 :: [a] -> [a]
reverse_bug_1 [] = []
reverse_bug_1 l = reverse l ++ [head l]

reverse_bug_2 :: [a] -> [a]
reverse_bug_2 (x:_:xs) = reverse xs ++ [x,x]
reverse_bug_2 l = reverse l

listPropertiesMain :: IO ()
listPropertiesMain = do
  let qcName name prop = do
        putStr $ name ++ ": "
        quickCheck prop

  putStrLn "The following tests should all succeed:"
  qcName "const"     $ prop_const     (const     :: Char -> Char -> Char)
  qcName "minimum"   $ prop_minimum   (minimum   :: [Char] -> Char)
  qcName "replicate" $ prop_replicate (replicate :: Int -> Char -> [Char])
  qcName "group_1"   $ prop_group_1   (group     :: [Char] -> [[Char]])
  qcName "group_2"   $ prop_group_2   (group     :: [Char] -> [[Char]])
  qcName "reverse_1" $ prop_reverse_1 (reverse   :: [Char] -> [Char])
  qcName "reverse_2" $ prop_reverse_2 (reverse   :: [Char] -> [Char])

  putStrLn ""

  putStrLn "The following tests should all fail:"
  qcName "const"     $ prop_const     (const_bug     :: Char -> Char -> Char)
  qcName "minimum"   $ prop_minimum   (minimum_bug   :: [Char] -> Char)
  qcName "replicate" $ prop_replicate (replicate_bug :: Int -> Char -> [Char])
  qcName "group_1"   $ prop_group_1   (group_bug     :: [Char] -> [[Char]])
  qcName "group_2"   $ prop_group_2   (group_bug     :: [Char] -> [[Char]])
  qcName "reverse_1" $ prop_reverse_1 (reverse_bug_1 :: [Char] -> [Char])
  qcName "reverse_2" $ prop_reverse_2 (reverse_bug_2 :: [Char] -> [Char])

------------------------------------------------------------------------------
-- Problem 2: Using QuickCheck to debug a SAT solver

---------------------------------------------------------------------------
-- Basic types

-- | An expression in CNF (conjunctive normal form) is a conjunction
-- of clauses
type CNF = [ Clause ]

-- | A clause is a disjunction of a number of literals
type Clause = [ Lit ]

-- | A literal is either a positive or a negative variable
data Lit = Lit Bool Var deriving (Eq, Ord, Show)

instance Arbitrary Lit where
  arbitrary = liftM2 Lit arbitrary arbitrary
  shrink (Lit b v) = map (flip Lit v) (shrink b) ++
                     map (Lit b) (shrink v)

-- | Variables
data Var = A | B | C | D | E | F
  deriving (Read, Eq, Ord, Show, Enum, Bounded)

var :: Lit -> Var
var (Lit _ x) = x

instance Arbitrary Var where
  arbitrary = arbitraryBoundedEnum
  shrink A = []
  shrink x = enumFromTo A $ pred x

-- | Is the literal positive?
isPos :: Lit -> Bool
isPos (Lit b _) = b

-- | invert the polarity of a literal
invert :: Lit -> Lit
invert (Lit b x) = Lit (not b) x

exampleFormula :: CNF
exampleFormula = [[Lit True A, Lit True B, Lit True C],
                  [Lit False A],
                  [Lit False B, Lit True C]]

unSatFormula :: CNF
unSatFormula = [[Lit True A],
                [Lit False A]]

-- | Assignments of values to (some) variables
type Valuation = Map Var Bool

emptyValuation :: Valuation
emptyValuation = Map.empty

fromList :: [(Var,Bool)] -> Valuation
fromList = Map.fromList

exampleValuation :: Valuation
exampleValuation = Map.fromList [(A,False), (B,True), (C,True)]

litSatisfied :: Valuation -> Lit -> Bool
litSatisfied a (Lit b v) = Map.member v a && (b == a Map.! v)

satisfiedBy :: CNF -> Valuation -> Bool
satisfiedBy p a = all (any $ litSatisfied a) p

prop_satisfiedBy :: Bool
prop_satisfiedBy = exampleFormula `satisfiedBy` exampleValuation

extend :: Var -> Bool -> Valuation -> Valuation
extend = Map.insert

value :: Var -> Valuation -> Maybe Bool
value = Map.lookup

---------------------------------------------------------------------------
-- Simple SAT Solver

allVars :: [Var]
allVars = [minBound .. maxBound]

allValuations :: [Valuation]
allValuations = allValAux allVars [] where
  allValAux [] _ = [emptyValuation]
  allValAux (v:vs) vl = map (extend v True) (allValAux vs vl)
                          ++ map (extend v False) (allValAux vs vl)

prop_allValuations :: Bool
prop_allValuations = length allValuations == 2 ^ length allVars
                     && allElementsDistinct allValuations

allElementsDistinct :: Eq a => [a] -> Bool
allElementsDistinct []     = True
allElementsDistinct (x:xs) = all (x /=) xs &&
                             allElementsDistinct xs

sat0 :: CNF -> Maybe Valuation
sat0 c = find (satisfiedBy c) allValuations

prop_satResultCorrect :: (CNF -> Maybe Valuation) -> Property
prop_satResultCorrect solver =
  forAll arbitrary $ \p -> case solver p of
                               Just a  -> p `satisfiedBy` a
                               Nothing -> True

---------------------------------------------------------------------------
-- Instantiation

instantiate :: CNF -> Var -> Bool -> CNF
instantiate c v b = filterClause (filterLiteral c v b) v b where
  val = fromList[(v,b)]
  filterClause c' _ _ = filter (not.(any $ litSatisfied val)) c'
  filterLiteral c' _ _ = map (filter (litPred $ val)) c'
  litPred a l@(Lit _ v') = (not $ Map.member v' a) || litSatisfied a l


prop_instantiate :: CNF -> Var -> Bool
prop_instantiate c v = is_sat c == (is_sat $ instantiate c v True) ||
                                   (is_sat $ instantiate c v False)
                       where
                        is_sat c' =
                          case sat0 c' of
                            Just _ -> True
                            Nothing -> False


sat1 :: CNF -> Maybe Valuation
sat1 = sat where
  sat c = if null c then Just emptyValuation
          else if any null c then Nothing
          else
            let first = var $ head $ head c in
            case (sat $ try first True, sat $ try first False) of
              (Nothing, Nothing) -> Nothing
              (Just m, _) -> Just $ extend first True m
              (_, Just m) -> Just $ extend first False m
            where
              try = instantiate c

prop_sat1 :: CNF -> Bool
prop_sat1 s = (isJust $ sat1 s) == (isJust $ sat0 s)

---------------------------------------------------------------------------
-- Unit propagation

simplifyUnitClause :: CNF -> Maybe (CNF, Var, Bool)

-- 1) If (simplifyUnitClause s) returns Nothing, then there
--    are no remaining unit clauses in s.
-- 2) If it returns (Just s'), then s' is satisfiable iff s is.
prop_simplifyUnitClause :: CNF -> Bool
prop_simplifyUnitClause s =
  case simplifyUnitClause s of
    Nothing -> all (\x -> length x /= 1) s
    Just (s',_,_) -> (isJust $ sat0 s') == (isJust $ sat0 s)

unitClauses :: CNF -> [Lit]
unitClauses = (map head) . filter (\x -> length x == 1)

simplifyUnitClause c =
  case unitClauses c of
    [] -> Nothing
    (Lit b v:_) -> Just (instantiate c v b, v, b)

sat2 :: CNF -> Maybe Valuation
sat2 = sat where
  sat c = if null c then Just emptyValuation
          else if any null c then Nothing
          else
            case simplifyUnitClause c of
              Just (s,v,b) -> case sat s of
                                Nothing -> Nothing
                                Just m -> Just $ extend v b m
              Nothing -> let first = var $ head $ head c in
                         case try first of
                           (Nothing, Nothing) -> Nothing
                           (Just m, _) -> Just $ extend first True m
                           (_, Just m) -> Just $ extend first False m
                         where
                          try x =
                            (sat $ instantiate c x True,
                              sat $ instantiate c x False)

prop_sat2 :: CNF -> Bool
prop_sat2 s = (isJust $ sat2 s) == (isJust $ sat0 s)

---------------------------------------------------------------------------
-- Pure literal elimination

simplifyPureLiteral :: CNF -> Maybe (CNF, Var, Bool)

-- 1) If (simplifyPureLiteral s) returns Nothing, then there
--    are no remaining pure literals in s
-- 2) If it returns (Just s'), then s' is satisfiable iff s is
prop_simplifyPureLiteral :: CNF -> Bool
prop_simplifyPureLiteral s =
  case simplifyPureLiteral s of
    Nothing -> all (\x -> hasOpposite x flat) flat
    Just (s',_,_) -> (isJust $ sat0 s') == (isJust $ sat0 s)
  where
    flat = concat s
    hasOpposite x l =
      any (== Lit (not $ isPos x) (var x)) l
pureLiterals :: CNF -> [(Var,Bool)]
pureLiterals c =
  nub $ map convert $ filter (\x -> matches x literals) literals
  where literals = concat c
        matches x l =
          all (\y -> (x==y) || (var y /= var x)) l
        convert l = (var l, isPos l)

simplifyPureLiteral c =
  case pureLiterals c of
    [] -> Nothing
    (v, b) : _ -> Just (instantiate c v b, v, b)


-- The final DPLL algorithm:
dpll :: CNF -> Maybe Valuation
dpll = sat where
  sat c = if null c then Just emptyValuation
          else if any null c then Nothing
          else
            case (simplifyUnitClause c, simplifyPureLiteral c) of
              (Just (s,v,b), _) -> check s v b
              (_, Just (s,v,b)) -> check s v b
              _                 -> let first = var $ head $ head c in
                                   case try first of
                                     (Nothing, Nothing) -> Nothing
                                     (Just m, _) -> Just $ extend first True m
                                     (_, Just m) -> Just $ extend first False m
          where
          try x =
            (sat $ instantiate c x True, sat $ instantiate c x False)
          check s v b =
            case sat s of
              Nothing -> Nothing
              Just m -> Just $ extend v b m

prop_dpll :: CNF -> Bool
prop_dpll s = (isJust $ dpll s) == (isJust $ sat0 s)

------------------------------------------------------------------------------
-- Using QC as a SAT solver

instance Arbitrary Valuation where
  arbitrary = liftM fromList $ foldr (liftM2 (:)) (return []) genPairs where
    genPairs = (map (\v -> liftM2 (,) (return v) arbitrary) allVars)

prop_isSatisfiable :: CNF -> Valuation -> Property
prop_isSatisfiable c v =  expectFailure $ not (satisfiedBy c v)
------------------------------------------------------------------------------
-- All the tests in one convenient place:

main :: IO ()
main = quickCheck $    prop_satisfiedBy
                  .&&. prop_satResultCorrect sat0
                  .&&. prop_instantiate
                  .&&. prop_sat1
                  .&&. prop_satResultCorrect sat1
                  .&&. prop_simplifyUnitClause
                  .&&. prop_sat2
                  .&&. prop_satResultCorrect sat2
                  .&&. prop_simplifyPureLiteral
                  .&&. prop_dpll
                  .&&. prop_satResultCorrect dpll
