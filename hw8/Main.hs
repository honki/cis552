-- by Raul Martinez mraul
--    Honk Kim honki

{-# OPTIONS -Wall -fno-warn-orphans -fwarn-tabs -fno-warn-type-defaults #-}


module Main where

import FunSyntax 
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Monoid
import Control.Monad 
import Test.HUnit
import Test.QuickCheck()

data Cont r a = Cont { runCont :: (a -> r) -> r }

instance Monad (Cont r) where
   return x = Cont (\k -> k x)
   m >>= f  = Cont (\k -> runCont m (\a -> runCont (f a) k))

even_cps :: Int -> Cont r Bool
even_cps x = return (x `mod` 2 == 0)

prop_even :: Int -> Bool
prop_even x = runCont (even_cps x) id == even x

-- a

add_cps :: Int -> Int -> Cont r Int
add_cps x y = return (x + y)

prop_add :: Int -> Int -> Bool
prop_add x y = runCont (add_cps x y) id == x + y

-- b

map_cps :: (a -> Cont r b) -> [a] -> Cont r [b]
map_cps _ []   = return []
map_cps f (a:as) = do b <- f a
                      bs <- map_cps f as
                      return $ b:bs

prop_map :: [Int] -> Bool
prop_map x = runCont (map_cps even_cps x) id == map even x

-- c

filter_cps :: (a -> Cont r Bool) -> [a] -> Cont r [a] 
filter_cps _ []     = return []
filter_cps p (a:as) = do b <- p a
                         bs <- filter_cps p as
                         return $ if b then a:bs else bs

prop_filter :: [Int] -> Bool
prop_filter x = runCont (filter_cps even_cps x) id == filter even x

-- d

foldr_cps :: (a -> b -> Cont r b) -> b -> [a] -> Cont r b
foldr_cps _ b []     = return b
foldr_cps f b (a:as) = do ac <- foldr_cps f b as 
                          f a ac

prop_foldr :: Int -> [Int] -> Bool
prop_foldr b l = runCont (foldr_cps add_cps b l) id == foldr (+) b l

instance Monoid r => MonadPlus (Cont r) where
   mzero       = Cont (\_ -> mempty)
   mplus c1 c2 = Cont (\k -> runCont c1 k `mappend` runCont c2 k)

amb :: MonadPlus m => [a] -> m a
amb xs = msum (map return xs)

evens :: [Int]
evens = runCont cont (\x -> [x]) where
  cont :: Cont [Int] Int
  cont = do
    x <- amb [1,2,3,4,5,6]
    guard (even x)
    return x

multipleDwelling :: Cont [r] (Int,Int,Int,Int,Int)
multipleDwelling = do
  baker    <- amb [1 .. 5]
  cooper   <- amb [1 .. 5]
  fletcher <- amb [1 .. 5]
  miller   <- amb [1 .. 5] 
  smith    <- amb [1 .. 5]
  guard (distinct [baker, cooper, fletcher, miller, smith])
  guard (baker /= 5)
  guard (cooper /= 1)
  guard (fletcher /= 5)
  guard (fletcher /= 1)
  guard (miller > cooper)
  guard (not (adjacent smith fletcher))
  guard (not (adjacent fletcher cooper))
  return (baker, cooper, fletcher, miller, smith)

distinct :: Eq a => [a] -> Bool
distinct [] = True
distinct (x:xs) = all (x /=) xs && distinct xs

adjacent :: (Num a, Eq a) => a -> a -> Bool
adjacent x y = abs (x - y) == 1

sols :: Cont [a] a -> [a]
sols = flip runCont return -- Same as sols c = runCont c (x -> [x])

numSols :: Cont [a] a -> Int
numSols = length . sols

nMDsols :: Int
nMDsols = numSols multipleDwelling

-- a )

-- multipleDwelling without requirement that smith and fletcher live
-- on non adjacent floors. This problem now has 5 possible solutions
multipleDwelling' :: Cont [r] (Int,Int,Int,Int,Int)
multipleDwelling' = do
  baker    <- amb [1 .. 5]
  cooper   <- amb [1 .. 5]
  fletcher <- amb [1 .. 5]
  miller   <- amb [1 .. 5] 
  smith    <- amb [1 .. 5]
  guard (distinct [baker, cooper, fletcher, miller, smith])
  guard (baker /= 5)
  guard (cooper /= 1)
  guard (fletcher /= 5)
  guard (fletcher /= 1)
  guard (miller > cooper)
  guard (not (adjacent fletcher cooper))
  return (baker, cooper, fletcher, miller, smith)

-- b )

guardExample :: Cont [r] (Int, Int)
guardExample = do
  a <- amb [1..100]
  b <- amb [1..100]
  -- *
  guard $ distinct [a, b]
  -- **
  guard $ (a == 0 || a == 1)
  return (a, b)

-- | In guardExample above we define 1 <= a <= 100, 1 <= b <= 100 yielding
--   10000 possibilities initially. It then gets cut down to 9900 and then to
--   99. Here we rewrite it so that we define 1 <= a <= 100 yielding 100
--   possibilities for a which we then cut down to 1 possibility for a. We then
--   introduce b yielding now only 100 possibilities for solutions which we then 
--   cut down to 99. We never consider ~10000 possibilities as before.

guardExample' :: Cont [r] (Int, Int)
guardExample' = do
  a <- amb [1..100]
  guard $ (a == 0 || a == 1)
  b <- amb [1..100]
  guard $ distinct [a, b]
  return (a, b)

-- | We reorder the amb assignments and guards in order to cut down the number of 
--   possible solutions as early as possible
multipleDwellingRevised :: Cont [r] (Int,Int,Int,Int,Int)
multipleDwellingRevised = do
  fletcher <- amb [1 .. 5]
  guard (fletcher /= 5)
  guard (fletcher /= 1)
 
  cooper <- amb [1 .. 5]
  guard (cooper /= 1)
  guard (not (adjacent fletcher cooper))

  smith  <- amb [1 .. 5]
  guard (not (adjacent smith fletcher))

  miller <- amb [1 .. 5] 
  guard (miller > cooper)

  baker  <- amb [1 .. 5]
  guard (baker /= 5)

  guard (distinct [baker, cooper, fletcher, miller, smith])
  return (baker, cooper, fletcher, miller, smith)

-- c )

data Father = MrMoore 
    | ColonelDowning 
    | MrHall 
    | SirBarnacleHood 
    | DrParker 
        deriving (Enum, Eq, Ord, Bounded, Show)

data Daughter = MaryAnn | Gabrielle | Lorna | Rosalind | Melissa 
   deriving (Enum, Eq, Ord, Bounded, Show)

type Yacht = Daughter

yachts :: Cont [r] (Map Father Daughter, Map Father Yacht)
yachts = do
  dmap <- amb mapping
  guard $ dmap Map.! MrMoore                   == MaryAnn
  guard $ dmap Map.! MrMoore                   /= Lorna
  guard $ dmap Map.! SirBarnacleHood           == Melissa
  guard $ dmap Map.! MrHall                    /= Rosalind
  guard $ dmap Map.! ColonelDowning            /= Melissa
  ymap <- amb mapping
  guard $ ymap Map.! SirBarnacleHood           == Gabrielle
  guard $ ymap Map.! MrMoore                   == Lorna
  guard $ ymap Map.! MrHall                    == Rosalind
  guard $ ymap Map.! ColonelDowning            == Melissa
  guard $ ymap Map.! fatherOf dmap Gabrielle   == dmap Map.! DrParker
  return (dmap, ymap)

mapping :: [Map Father Daughter]
mapping = map Map.fromList $ do
  d1 <- [MaryAnn ..]
  d2 <- [MaryAnn ..]
  d3 <- [MaryAnn ..]
  d4 <- [MaryAnn ..]
  d5 <- [MaryAnn ..]
  guard $ distinct [d1,d2,d3,d4,d5]
  return $ zip [MrMoore ..] [d1,d2,d3,d4,d5]

mappingTests :: Test
mappingTests = TestList [
  length mapping ~?= 120,
  distinct (Map.keys (head mapping)) ~?= True,
  distinct (map snd (Map.assocs (head mapping))) ~?= True
  ]

fatherOf :: Map Father Daughter -> Daughter -> Father
fatherOf dmap d' = head [f | (f,d) <- Map.assocs dmap, (d == d')]

fatherOfTest :: Test
fatherOfTest = fatherOf (Map.fromList [(MrMoore, MaryAnn)]) MaryAnn ~?= MrMoore

-- Lorna's Father: ColonelDowning
-- Number of solutions if we didn't know Mary Ann's last name: 2

abort :: r -> Cont r a
abort r = Cont (\_ -> r)

data Value =
   IntVal  Int
 | BoolVal Bool
 | FunVal (Value -> Cont Value Value)
 -- new! specialized values for signaling errors
 | ErrorVal Error

data Error = BooleanOpMismatch Bop Value Value
           | ParseError String
           | UnboundVar Variable
           | BooleanValMismatch Value
           | FunctionValMismatch Value
           | LetRecExprMismatch Expression
 -- you will need to add other constructors to describe the various runtime 
 -- errors from the interpreter
      
  deriving (Show, Eq)

instance Show Value where
   show (IntVal i)   = show i
   show (BoolVal b)  = show b
   show (FunVal _)   = "<function>"
   show (ErrorVal e) = show e

instance Eq Value where
   IntVal i == IntVal j = i == j
   BoolVal i == BoolVal j = i == j
   FunVal _ == FunVal _ = error "Functions cannot be compared"
   ErrorVal e == ErrorVal f = e == f
   _ == _ = False

evalB :: Bop -> Value -> Value -> Cont Value Value
evalB Plus   (IntVal i1) (IntVal i2) = return $ IntVal  (i1 + i2)
evalB Minus  (IntVal i1) (IntVal i2) = return $ IntVal  (i1 - i2)
evalB Times  (IntVal i1) (IntVal i2) = return $ IntVal  (i1 * i2)
evalB Gt     (IntVal i1) (IntVal i2) = return $ BoolVal (i1 > i2)
evalB Ge     (IntVal i1) (IntVal i2) = return $ BoolVal (i1 >= i2)
evalB Lt     (IntVal i1) (IntVal i2) = return $ BoolVal (i1 < i2)
evalB Le     (IntVal i1) (IntVal i2) = return $ BoolVal (i1 <= i2)
evalB b v1 v2 = abort (ErrorVal (BooleanOpMismatch b v1 v2))

data Environment = Environment 
   { vars :: Map Variable Value, handlers :: [Handler] }

extend :: Variable -> Value -> Environment -> Environment
extend x v s = s { vars = Map.insert x v (vars s) }

pushHandler :: Handler -> Environment -> Environment
pushHandler h s = s { handlers = h : (handlers s) }

type Handler = Value -> Value

evalK :: Expression -> Environment -> Cont Value Value 
evalK (Var x) s = case Map.lookup x (vars s) of
                    Just v  -> return v
                    Nothing -> abort $ ErrorVal (UnboundVar x) 
evalK (IntExp i) _ = return $ IntVal i
evalK (BoolExp b) _ = return $ BoolVal b
evalK (Op bop e1 e2) s = do
  v1 <- evalK e1 s
  v2 <- evalK e2 s
  evalB bop v1 v2
evalK (If e1 e2 e3) s = do
  v1 <- evalK e1 s
  case v1 of
    BoolVal b -> if b then evalK e2 s else evalK e3 s
    _         -> abort $ ErrorVal (BooleanValMismatch v1)
evalK (Fun x e) s = return $ FunVal (\v -> evalK e (extend x v s)) 
evalK (App f a) s = do
  fv <- evalK f s
  av <- evalK a s
  case fv of
    FunVal fun -> fun av
    _          -> abort $ ErrorVal (FunctionValMismatch fv)
evalK (LetRec x (Fun y e1) e2) s = do
  let v = runCont (evalK (Fun y e1) s') id
      s' = extend x v s
  evalK e2 s'
evalK (Throw e) s = do
  v <- evalK e s
  case handlers s of
    (x:_) -> return $ x v
    []    -> abort v
evalK (Try e1 var e2) s = do
  let s' = pushHandler (\v -> runCont (evalK e2 (extend var v s)) id) s
  evalK e1 s'
  
evalK (LetRec _ e _) _ = abort (ErrorVal (LetRecExprMismatch e))
  
 
eval :: Expression -> Value
eval e = runCont (evalK e (Environment Map.empty [])) id

re :: String -> Value
re line = 
   case parse line of 
     Just e -> eval e
     Nothing  -> ErrorVal (ParseError line)

tests :: Test
tests = TestList [ 
  re "throw 3" ~?= IntVal 3, 
  re "try throw 1 catch X with X + 2 endwith" ~?= IntVal 3,
  re "try 1 + 2 catch X with X + 3 endwith" ~?= IntVal 3,
  re "try (try throw 1 catch X with throw X + 1 endwith) catch Y with Y + 1 endwith" ~?= IntVal 3,
  re "try (try throw 1 catch X with X + 2 endwith) catch Y with Y + 4 endwith" ~?= IntVal 3]

abortTests :: Test
abortTests = TestList [
  re "X" ~?= ErrorVal (UnboundVar "X"),
  re "if 1 then 2 else 3" ~?= ErrorVal (BooleanValMismatch (IntVal 1)),
  re "1 2" ~?= ErrorVal (FunctionValMismatch (IntVal 1)),
  re "let rec X = 1 in 2" ~?= ErrorVal (LetRecExprMismatch (IntExp 1))
  ]

funcTests :: Test
funcTests = TestList [
  re "let rec FACT = fun X -> if X <= 1 then 1 else X * FACT (X - 1) in FACT 5" ~?= IntVal 120,
  re "(fun X -> fun Y -> if X > Y then X else Y) 10 20" ~?= IntVal 20
  ]

main :: IO ()
main = do
   _ <- runTestTT $ TestList [ tests, abortTests, funcTests, mappingTests, fatherOfTest ]
   return ()

repl :: IO ()
repl = do
   putStr "%> "
   line <- getLine
   putStrLn $ show (re line)
   repl

data RegExp = Char Char            -- single literal character
            | Alt RegExp RegExp    -- r1 | r2   (alternation)
            | Seq RegExp RegExp    -- r1 r2     (concatenation)
            | Star RegExp          -- r*        (Kleene star)
            | Empty                -- ε, accepts empty string
            | Void                 -- ∅, always fails 
  deriving (Eq, Show)



acc :: RegExp -> String -> Cont Any String

acc = undefined

accept :: RegExp -> String -> Bool
accept r s = getAny $ runCont (acc r s) (Any . null)
