{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults  #-}

-- The basic definition of the parsing monad as developed in lecture.
-- Operations for building composite parsers are in the module
-- ParserCombinators.

module Parser (Parser, GenParser,
               get,
               choose,
               (<|>),
               satisfy,
               doParse,
              ) where

import Control.Monad

type Parser a = GenParser Char a

newtype GenParser tok a = P ([tok] -> [(a, [tok])])

doParse :: GenParser tok a -> [tok] -> [(a, [tok])]
doParse (P p) s = p s

-- | Return the next token
-- (this was called 'oneChar' in lecture)
get :: GenParser tok tok
get = P (\cs -> case cs of
                (x:xs) -> [ (x,xs) ]
                []     -> [])

-- | Return the next token if it satisfies the given predicate
-- (this was called satP in lecture)
satisfy :: (tok -> Bool) -> GenParser tok tok
satisfy p = do c <- get
               if (p c) then return c else fail "End of input"

instance Monad (GenParser tok) where
   p1 >>= fp2 = P (\cs -> do (a,cs') <- doParse p1 cs
                             doParse (fp2 a) cs')

   return x   = P (\cs -> [ (x, cs) ])

   fail _     = P (\_ ->  [ ])

instance Functor (GenParser tok) where
   fmap f p = do x <- p
                 return (f x)

instance MonadPlus (GenParser tok) where
  mzero = P (\_ -> [])

  mplus = choose

-- | Combine two parsers together in parallel, producing all
-- possible results from either parser.
choose :: GenParser tok a -> GenParser tok a -> GenParser tok a
p1 `choose` p2 = P (\cs -> doParse p1 cs ++ doParse p2 cs)

-- | Combine two parsers together in parallel, but only use the
-- first result. This means that the second parser is used only
-- if the first parser completely fails.
(<|>) :: GenParser tok a -> GenParser tok a -> GenParser tok a
p1 <|> p2 = P $ \cs -> case doParse (p1 `choose` p2) cs of
                          []   -> []
                          x:_ -> [x]
