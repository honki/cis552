-- Advanced Programming, HW 5
-- by <David Xu> <davix> <78152170>
--    <Hong Kim> <honki> <44458371>

{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults -fno-warn-orphans #-}
{-# LANGUAGE FlexibleInstances, OverlappingInstances #-}

module Main where

import Prelude hiding (or)

import Control.Monad

import Test.QuickCheck

import Text.PrettyPrint (Doc, (<+>))
import qualified Text.PrettyPrint as PP

import ParserTrans
import ParserCombinators

import Test.HUnit

type Variable = String

data Value =
    IntVal Int
  | BoolVal Bool
  deriving (Show, Eq)

data Expression =
    Var Variable
  | Val Value
  | Op  Bop Expression Expression
  deriving (Show, Eq)

data Bop =
    Plus
  | Minus
  | Times
  | Divide
  | Gt
  | Ge
  | Lt
  | Le
  deriving (Show, Eq)

data Statement =
    Assign Variable Expression
  | If Expression Statement Statement
  | While Expression Statement
  | Sequence Statement Statement
  | Skip
  deriving (Show, Eq)

-- Problem 0
---------------------------------------------

wFact :: Statement
wFact = Sequence (Assign "N" (Val (IntVal 2))) (Sequence (Assign "F" (Val (IntVal 1))) (While (Op Gt (Var "N") (Val (IntVal 0))) (Sequence (Assign "X" (Var "N")) (Sequence (Assign "Z" (Var "F")) (Sequence (While (Op Gt (Var "X") (Val (IntVal 1))) (Sequence (Assign "F" (Op Plus (Var "Z") (Var "F"))) (Assign "X" (Op Minus (Var "X") (Val (IntVal 1)))))) (Assign "N" (Op Minus (Var "N") (Val (IntVal 1)))))))))

class PP a where
  pp :: a -> Doc

oneLine :: PP a => a -> IO ()
oneLine = putStrLn . PP.renderStyle (PP.style {PP.mode=PP.OneLineMode}) . pp

indented :: PP a => a -> IO ()
indented = putStrLn . PP.render . pp

instance PP Bop where
  pp Plus   = PP.char '+'
  pp Minus  = PP.char '-'
  pp Times  = PP.char '*'
  pp Divide = PP.char '/'
  pp Gt     = PP.char '>'
  pp Ge     = PP.text ">="
  pp Lt     = PP.char '<'
  pp Le     = PP.text "<="

instance PP Value where
  pp (IntVal v) = PP.int v
  pp (BoolVal v)
    | v         = PP.text "true"
    | otherwise = PP.text "false"


precedence        :: Bop -> Int
precedence Plus   = 1
precedence Minus  = 1
precedence Times  = 2
precedence Divide = 2
precedence Gt     = 0
precedence Ge     = 0
precedence Lt     = 0
precedence Le     = 0

instance PP Expression where
  pp (Var x)      = PP.text x
  pp (Val v)      = pp v
  pp (Op f e1 e2) = ppl e1 f <+> pp f <+> ppr e2 f
    where ppl op@(Op g _ _) h
            | precedence h > precedence g = PP.parens $ pp op
            | otherwise                   = pp op

          ppl v             _ = pp v

          ppr op@(Op g _ _) h
            | precedence h >= precedence g = PP.parens $ pp op
            | otherwise                    = pp op

          ppr v             _ = pp v


instance PP Variable where
  pp = PP.text

indent :: Doc -> Doc
indent = PP.nest 2

instance PP Statement where
  pp (Assign v e)     = pp v <+> PP.text ":=" <+> pp e

  pp (If e s1 s2)     = let ifExpThen' = PP.text "if" <+> pp e <+> PP.text "then"
                            trueBlock  = pp s1
                            else'      = PP.text "else"
                            falseBlock = pp s2
                            endif'     = PP.text "endif"
                        in
                          PP.vcat [ ifExpThen'
                                  , indent trueBlock
                                  , else'
                                  , indent falseBlock
                                  , endif' ]
  pp (While e s)      = let whileExpDo' = PP.text "while" <+> pp e <+> PP.text "do"
                            block       = pp s
                            endwhile'   = PP.text "endwhile"
                        in
                          PP.vcat [ whileExpDo'
                                  , indent block
                                  , endwhile' ]
  pp (Sequence s1 s2) = PP.vcat $ PP.punctuate PP.semi [pp s1, pp s2]
  pp Skip             = PP.text "skip"

display :: PP a => a -> String
display = show . pp

-- Simple tests

v1,v2,v3 :: Expression
v1   = Val (IntVal 1)
v2   = Val (IntVal 2)
v3 = Val (IntVal 3)

t0 :: Test
t0 = TestList [ display v1 ~?= "1"
              , display (BoolVal True) ~?= "true"
              , display (Var "X") ~?= "X"
              , display (Op Plus v1 v2) ~?= "1 + 2"
              , display (Op Plus v1 (Op Plus v2 v3)) ~?= "1 + (2 + 3)"
              , display (Op Plus (Op Plus v1 v2) v3) ~?= "1 + 2 + 3"
              , display (Assign "X" v3) ~?= "X := 3"
              , display Skip ~?= "skip" ]


--- (Your own test cases go here...)

t0a :: Test
t0a = TestList [ display (Op Plus (Op Plus v1 v2) (Op Plus v2 v3))
                           ~?= "1 + 2 + (2 + 3)"

               , display (Op Plus (Op Plus v1 (Op Plus v1 v2)) v3)
                           ~?= "1 + (1 + 2) + 3"

               , display (Op Times v1 (Op Plus v2 v3))
                           ~?= "1 * (2 + 3)"

               , display (Op Times (Op Plus v3 v2) v1)
                           ~?= "(3 + 2) * 1"

               , display (Op Plus (Op Times v1 v2) v3)
                           ~?= "1 * 2 + 3" ]

t0b :: Test
t0b =
  TestList
  [ display (If (Val (BoolVal True)) Skip Skip)
              ~?= "if true then\n  skip\nelse\n  skip\nendif"

  , display (While (Val $ IntVal 17) (While (Val $ IntVal 17) Skip))
              ~?= "while 17 do\n  while 17 do\n    skip\n  endwhile\nendwhile"

  , display (Sequence (Sequence Skip Skip) Skip)
              ~?= "skip;\nskip;\nskip"
  , display (Sequence (Assign "X" (Val $ IntVal 17)) (Skip))
              ~?= "X := 17;\nskip" ]



------------------------------------------------------------------------
-- Problem 1

valueP :: Parser Value
valueP = intP <|> boolP

intP :: Parser Value
intP = do n <- int
          return $ IntVal n

constP     :: String -> a -> Parser a
constP s a = string s >> return a

boolP :: Parser Value
boolP = constP "true" (BoolVal True) <|> constP "false" (BoolVal False)

failP :: Parser a
failP = mzero

constsP :: [(String, a)] -> Parser a
constsP = foldr f failP
  where f (s, a) p = p <|> constP s a

symbolToBopList :: [(String, Bop)]
symbolToBopList = [ ("+", Plus)
                  , ("-", Minus)
                  , ("*", Times)
                  , ("/", Divide)
                  , (">", Gt)
                  , ("<", Lt)
                  , (">=", Ge)
                  , ("<=", Le) ]

opP :: Parser Bop
opP = constsP symbolToBopList

varP :: Parser Variable
varP = many1 upper

wsP :: Parser a -> Parser a
wsP p = do a <- p
           _ <- many space
           return a

-- | Parses and returns the given 'Char', with trailing whitespace removed.
charw :: Char -> Parser Char
charw = wsP . char

-- | Parses and returns the given 'String', with trailing whitespace removed.
stringw :: String -> Parser String
stringw = wsP . string

sumOpP, prodOpP, logicOpP :: Parser (Expression -> Expression -> Expression)
sumOpP   = do op <- opP
              guard $ elem op [Plus, Minus]
              return $ Op op

prodOpP  = do op <- opP
              guard $ elem op [Times, Divide]
              return $ Op op

logicOpP = do op <- opP
              guard $ elem op [Ge, Le, Gt, Lt]
              return $ Op op

exprP, sumEP, prodEP, factorEP :: Parser Expression
exprP    = wsP sumEP `chainl1` wsP logicOpP
sumEP    = wsP prodEP `chainl1` wsP sumOpP
prodEP   = wsP factorEP `chainl1` wsP prodOpP
factorEP = between (wsP (char '(')) exprP (wsP (char ')'))
          <|> liftM Var varP
          <|> liftM Val valueP

t11 :: Test
t11 = TestList ["s1" ~: succeed (parse exprP "1 "),
                "s2" ~: succeed (parse exprP "1  + 2") ] where
  succeed (Left _)  = assert False
  succeed (Right _) = assert True

runRightTest           :: Either String Test -> Test
runRightTest (Left e)  = test $ assertFailure e
runRightTest (Right t) = t

t11a :: Test
t11a = TestList [ "1___"      ~: "1   "      `parsesTo` v1
                , "1+2"       ~: "1+2"       `parsesTo` Op Plus v1 v2
                , "1+___2___" ~: "1+   2   " `parsesTo` Op Plus v1 v2
                , "1___+2"    ~: "1   +2"    `parsesTo` Op Plus v1 v2
                , "1_+_2___"  ~: "1 + 2   "  `parsesTo` Op Plus v1 v2
                , "(1+2)"     ~: "(1 + 2)"   `parsesTo` Op Plus v1 v2
                , "(_1_+_2_)" ~: "( 1 + 2 )" `parsesTo` Op Plus v1 v2
                , "(1+2__)__" ~: "(1+2  )  " `parsesTo` Op Plus v1 v2 ]
  where s `parsesTo` ast = runRightTest $ do ast' <-parse exprP s
                                             return $ ast' ~?= ast


t11b :: Test
t11b =
  TestList [ "(1+2)*3" ~: "(1+2)*3" `parsesTo` Op Times (Op Plus v1 v2) v3
           , "1+2*3"   ~: "1+2*3"   `parsesTo` Op Plus v1 (Op Times v2 v3) ]

  where s `parsesTo` ast = case parse exprP s of
                             Left e     -> test $ assertFailure e
                             Right ast' -> ast' ~?= ast

statementP :: Parser Statement
statementP = choice [ sequenceP, assignP, ifP, whileP, skipP ]
  where assignP   = liftM3 (\v _ e -> Assign v e)
                    (wsP varP) (wsP $ string ":=") exprP

        ifP       = do _ <- stringw "if"
                       e <- exprP
                       _ <- stringw "then"
                       s <- wsP statementP
                       _ <- stringw "else"
                       t <- wsP statementP
                       _ <- stringw "endif"
                       return $ If e s t

        whileP    = do _ <- stringw "while"
                       e <- exprP
                       _ <- stringw "do"
                       s <- wsP statementP
                       _ <- stringw "endwhile"
                       return $ While e s

        skipP     = wsP $ constP "skip" Skip

        sequenceP = do s1 <- notSequenceP
                       _  <- charw ';'
                       s2 <- statementP
                       return $ Sequence s1 s2

        notSequenceP = choice [ assignP, ifP, whileP, skipP ]

t12 :: Test
t12 = TestList ["s1" ~: p "fact.imp",
                "s2" ~: p "test.imp",
                "s3" ~: p "abs.imp" ,
                "s4" ~: p "times.imp" ] where
  p s = parseFromFile statementP s >>= succeed
    -- Or: p = succeed <=< parseFromFile statementP
  succeed (Left _)  = assert False
  succeed (Right _) = assert True

testRT :: String -> Assertion
testRT filename = do
   x <- parseFromFile statementP filename
   case x of
     Right ast -> case parse statementP (display ast) of
       Right ast' -> assert (ast == ast')
       Left _ -> assert False
     Left _ -> assert False

t13 :: Test
t13 = TestList ["s1" ~: testRT "fact.imp",
                "s2" ~: testRT "test.imp",
                "s3" ~: testRT "abs.imp" ,
                "s4" ~: testRT "times.imp" ]

instance Arbitrary Variable where
  arbitrary = elements $ map (:[]) ['A'..'Z']
  shrink (x:_) = [shrink x]
  shrink []    = [] -- won't get here

instance Arbitrary Value where
  arbitrary = oneof [ liftM IntVal arbitrary
                    , liftM BoolVal arbitrary ]
  shrink (IntVal n)  = map IntVal $ shrinkIntegral n
  shrink (BoolVal _) = []

instance Arbitrary Bop where
  arbitrary = oneof $ map return [ Plus, Minus, Times, Divide, Gt, Ge, Lt, Le ]

instance Arbitrary Expression where
  arbitrary = sized arbn
    where arbn n = frequency [ (1, liftM Var arbitrary),
                               (1, liftM Val arbitrary),
                               (n, liftM3 Op arbitrary arb arb) ]
            where arb = arbn $ n `div` 2

  shrink (Var _)    = []
  shrink (Val _)    = []
  shrink (Op _ e f) = shrink e ++ shrink f

instance Arbitrary Statement where
  arbitrary = sized arbn
    where arbn n = let seqF    = (n, liftM2 Sequence (frequency notSeqF) arb)

                       notSeqF = [ (n, liftM2 Assign arbitrary arbitrary)
                                 , (n, liftM3 If arbitrary arb arb)
                                 , (n, liftM2 While arbitrary arb)
                                 , (1, return Skip) ]

                       arb = arbn $ n `div` 2

                   in  frequency $ seqF : notSeqF

  shrink (Assign _ _)         = [Skip]
  shrink (If _ s t)           = Skip : shrink s ++ shrink t
  shrink (While _ s)          = Skip : shrink s
  shrink (Sequence Skip Skip) = [Skip]
  shrink (Sequence s    t)    = Skip : shrink s ++  shrink t
  shrink Skip                 = [Skip]

runRightBool           :: Either a Bool -> Bool
runRightBool (Right b) = b
runRightBool (Left  _) = False

prop_roundtrip     :: Statement -> Bool
prop_roundtrip ast = runRightBool $ do ast' <- parse statementP $ display ast
                                       return $ ast' == ast


------------------------------------------------------------------------
-- Problem 2

data Token =
     TokVar String     -- variables
   | TokVal Value      -- primitive values
   | TokBop Bop        -- binary operators
   | Keyword String    -- keywords
      deriving (Eq, Show)

keywords :: [Parser Token]
keywords = map (\x -> constP x (Keyword x))
             [ "(", ")", ":=", ";", "if", "then", "else",
             "endif", "while", "do", "endwhile", "skip" ]

type Lexer = Parser [Token]

lexer :: Lexer
lexer = sepBy1
        (liftM TokVal valueP <|>
         liftM TokVar varP   <|>
         liftM TokBop opP    <|>
         choice keywords)
        (many space)

t2 :: Test
t2 = parse lexer "X := 3" ~?=
        Right [TokVar "X", Keyword ":=", TokVal (IntVal 3)]

type TokParser a = GenParser Token a

intTP :: TokParser Value
intTP = do TokVal v@(IntVal _) <- getC
           return v

boolTP :: TokParser Value
boolTP = do TokVal v@(BoolVal _) <- getC
            return v

valueTP :: TokParser Value
valueTP = intTP <|> boolTP

varTP :: TokParser Variable
varTP = do TokVar x <- getC
           return x

failTP :: TokParser a
failTP = mzero

opTP :: TokParser Bop
opTP = do TokBop bop <- getC
          return bop

sumOpTP, prodOpTP, logicOpTP :: TokParser (Expression -> Expression -> Expression)
sumOpTP   = do op <- opTP
               guard $ elem op [Plus, Minus]
               return $ Op op

prodOpTP  = do op <- opTP
               guard $ elem op [Times, Divide]
               return $ Op op

logicOpTP = do op <- opTP
               guard $ elem op [Ge, Le, Gt, Lt]
               return $ Op op

exprTP, sumETP, prodETP, factorETP :: TokParser Expression
exprTP    = sumETP `chainl1` logicOpTP
sumETP    = prodETP `chainl1` sumOpTP
prodETP   = factorETP `chainl1` prodOpTP
factorETP = between (keywordTP "(") exprTP (keywordTP ")")
            <|> liftM Var varTP
            <|> liftM Val valueTP

keywordTP :: String -> TokParser String
keywordTP s = do Keyword k <- getC
                 guard $ s == k
                 return k

statementTP :: TokParser Statement
statementTP = choice [ sequenceTP, assignTP, ifTP, whileTP, skipTP ]
  where assignTP = liftM3 (\v _ e -> Assign v e) varTP (keywordTP ":=") exprTP
        ifTP     = do _ <- keywordTP "if"
                      e <- exprTP
                      _ <- keywordTP "then"
                      s <- statementTP
                      _ <- keywordTP "else"
                      t <- statementTP
                      _ <- keywordTP "endif"
                      return $ If e s t

        whileTP = do _ <- keywordTP "while"
                     e <- exprTP
                     _ <- keywordTP "do"
                     s <- statementTP
                     _ <- keywordTP "endwhile"
                     return $ While e s

        skipTP  = keywordTP "skip" >> return Skip

        sequenceTP = do s1 <- notSequenceTP
                        _  <- keywordTP ";"
                        s2 <- statementTP
                        return $ Sequence s1 s2

        notSequenceTP = choice [ assignTP, ifTP, whileTP, skipTP ]

prop_groundtrip :: Statement -> Bool
prop_groundtrip ast = runRightBool $ do toks <- parse lexer $ display ast
                                        ast' <- parse statementTP toks
                                        return $ ast' == ast

-----------------------------------------------------------------
-- A main action to run all the tests...

main :: IO ()
main = do _ <- runTestTT $ TestList [ t0, t0a, t0b,
                                      t11, t11a, t11b, t12, t13 ]
          _ <- quickCheck prop_roundtrip
          _ <- runTestTT $ TestList [ t2 ]
          _ <- quickCheck prop_groundtrip
          return ()
