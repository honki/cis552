> {-# OPTIONS -Wall -fwarn-tabs #-}
>
> module FunctorMonadLaws where
>
> import Test.QuickCheck.Function

In this file, we present the laws for functors and monads as QuickCheck
properties.  One feature of this file is that you'll notice that we've replaced
function types like `a -> b` with [`Fun a
b`](http://hackage.haskell.org/package/QuickCheck-2.6/docs/Test-QuickCheck-Function.html);
this is a type that allows QuickCheck to generate arbitrary functors.  When
using it, the second argument to the constructor is the function you want to
apply (the first is machinery for generation).

The functor laws
================

Functor instances should always satisfy two laws.

The first functor law says that mapping `id` inside the functor is the same as
applying `id` to the outside directly, *i.e.* it's the same as doing nothing"

> prop_FMapId :: (Eq (f a), Functor f) => f a -> Bool
> prop_FMapId x = fmap id x == id x

The second law allows us to combine two passes with `fmap` into a single one
using function composition:

> prop_FMapComp :: (Eq (f c), Functor f) => Fun b c -> Fun a b -> f a -> Bool
> prop_FMapComp (Fun _ f) (Fun _ g) x =
>    fmap (f . g) x == (fmap f . fmap g) x

The monad laws
==============

Likewise, monad instances should satisfy the *three* monad laws.

First, `return` is a no-op computation on the left of bind:

> prop_LeftUnit :: (Eq (m b), Monad m) => a -> Fun a (m b) -> Bool
> prop_LeftUnit x (Fun _ f) = 
>    (return x >>= f) == f x

Second, `return` is a no-op computation on the right of bind:

> prop_RightUnit :: (Eq (m b), Monad m) => m b -> Bool
> prop_RightUnit m = 
>    (m >>= return) == m

And third, it doesn't matter in which order we bind things:

> prop_Assoc :: (Eq (m c), Monad m)
>            => m a -> Fun a (m b) -> Fun b (m c) -> Bool
> prop_Assoc m (Fun _ f) (Fun _ g) =
>    ((m >>= f) >>= g) == (m >>= \x -> f x >>= g)

> prop_Monad :: (Eq (m b), Eq (m a), Eq (m c), Monad m)
>            => a -> m a -> Fun a (m b) -> Fun b (m c) -> Bool
> prop_Monad a ma fab fbc =
>   prop_LeftUnit  a fab      &&
>   prop_RightUnit ma         &&
>   prop_Assoc     ma fab fbc

Functors and monads together
============================

Finally, types that are instances of both `Functor` and `Monad` should satisfy
one additional law, which says that we can define `fmap` in terms of `>>=` and
`return`:

> prop_FunctorMonad :: (Eq (m b), Functor m, Monad m) => m a -> Fun a b -> Bool
> prop_FunctorMonad x (Fun _ f) = (fmap f x) == (x >>= return . f)
