-- Advanced Programming, HW 6
-- by Eric Kutschera, kute
--    Hong Kim, honki

{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults -fno-warn-orphans -Werror #-} 
{-# LANGUAGE FlexibleInstances, FlexibleContexts, MultiParamTypeClasses #-}

module MonadTransformers where
 
import Prelude hiding (Maybe, Just, Nothing)
import Control.Monad (liftM)
import Test.QuickCheck
import Test.QuickCheck.Function

import FunctorMonadLaws

data Maybe a = Just a
             | Nothing
             deriving (Show, Eq)
 
instance Functor Maybe where
  fmap f (Just x) = Just $ f x
  fmap _ Nothing  = Nothing
 
instance Monad Maybe where
  return        = Just
  Nothing >>= _ = Nothing
  Just x  >>= f = f x

instance Arbitrary a => Arbitrary (Maybe a) where
  arbitrary = frequency [ (1, return Nothing)
                        , (4, liftM Just arbitrary)
                        ]
  shrink Nothing  = []
  shrink (Just x) = Nothing : map Just (shrink x)

qc_Maybe :: IO ()
qc_Maybe = quickCheck (prop_Monad :: Char -> Maybe Char ->
                                     Fun Char (Maybe Int) ->
                                     Fun Int (Maybe String) -> Bool)

newtype MaybeT m a = MT { unMT :: m (Maybe a) }

instance Monad m => Monad (MaybeT m) where
  return = MT . return . Just
 
  MT x >>= f = MT $
    do ma <- x
       case ma of
         Just a -> unMT $ f a
         Nothing -> return Nothing

instance Eq a => Eq (MaybeT [] a) where
  MT x == MT y = x == y

instance Show a => Show (MaybeT [] a) where
  show (MT x) = show x

instance Arbitrary a => Arbitrary (MaybeT [] a) where
  arbitrary = liftM MT arbitrary
  shrink    = map MT . shrink . unMT

qc_MaybeT :: IO ()
qc_MaybeT = quickCheck $ (prop_Monad :: Char -> MaybeT [] Char ->
                                     Fun Char (MaybeT [] Int) ->
                                     Fun Int (MaybeT [] String) -> Bool)

data MaybeW m a = MW { unMaybeW :: Maybe (m a) }

instance Monad m => Monad (MaybeW m) where

  return = MW . Just . return

  MW Nothing  >>= _ = MW Nothing

  -- MW (Just x) >>= f = ??
  MW (Just _x) >>= _ = MW Nothing

instance Eq a => Eq (MaybeW [] a) where
  MW x == MW y = x == y

instance Show a => Show (MaybeW [] a) where
  show (MW x) = show x

instance Arbitrary a => Arbitrary (MaybeW [] a) where
  arbitrary = liftM MW arbitrary
  shrink    = map MW . shrink . unMaybeW

qc_MaybeW :: IO ()
qc_MaybeW = quickCheck $ (prop_Monad :: Char -> MaybeW [] Char ->
                                     Fun Char (MaybeW [] Int) ->
                                     Fun Int (MaybeW [] String) -> Bool)

class (Monad m, Monad (t m)) => MonadTrans t m where
  lift :: Monad m => m a -> t m a

instance Monad m => MonadTrans MaybeT m where
  lift ma = MT $
    do a <- ma
       return $ Just a

prop_MonadTransReturn :: Char -> Bool
prop_MonadTransReturn a = (lift . return) a == (return a :: MaybeT [] Char)

prop_MonadTransBind :: [Char] -> (Fun Char [Char]) -> Bool
prop_MonadTransBind m (Fun _ k) = lift (m >>= k) == (lift m >>= (lift . k) :: MaybeT [] Char)

main :: IO ()
main = do
  qc_Maybe
  qc_MaybeT
  putStrLn "Should Fail:"
  qc_MaybeW
  putStrLn "Should Pass:"
  quickCheck prop_MonadTransReturn
  quickCheck prop_MonadTransBind
