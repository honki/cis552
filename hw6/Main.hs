-- Advanced Programming, HW 6
-- by Eric Kutschera, kute
--    Hong Kim, honki

{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults -fno-warn-orphans  -Werror #-} 
{-# LANGUAGE TypeSynonymInstances, FlexibleContexts, NoMonomorphismRestriction, 
    FlexibleInstances #-}

module Main where

import WhilePP

import Data.Map (Map)
import qualified Data.Map as Map

import Control.Monad.State
import Control.Monad.Error
import Control.Monad.Writer
import Control.Monad.Identity

import Test.HUnit hiding (State)

type Store    = Map Variable Value
evalS :: (MonadState Store m, MonadError Value m, MonadWriter String m) => Statement -> m ()
evalS (Assign v e) = do
  s <- get
  val <- evalE e
  put $ Map.insert v val s
evalS (If e s1 s2) = do
  val <- evalE e
  case val of
    BoolVal b -> if b then evalS s1 else evalS s2
    _         -> throwError $ IntVal 2
evalS (While e s) =
  do evalS $ If e (Sequence s $ While e s) Skip
evalS (Sequence s1 s2) = evalS s1 >> evalS s2
evalS Skip = return ()
evalS (Print str e) = do
  val <- evalE e
  tell $ str ++ display val
evalS (Throw e) = do
  val <- evalE e
  throwError val
evalS (Try s x h) = evalS s `catchError`
                    (\v -> evalS $ Sequence (Assign x (Val v)) h)
 

evalE :: (MonadState Store m, MonadError Value m) => Expression -> m Value
evalE (Var v) = do
  s <- get
  case Map.lookup v s of
    Just val -> return val
    Nothing  -> throwError $ IntVal 0
evalE (Val v) = return v
evalE (Op b e1 e2) = do
  v1 <- evalE e1
  v2 <- evalE e2
  evalBop b v1 v2

  
evalBop :: MonadError Value m => Bop -> Value -> Value -> m Value
evalBop Plus (IntVal a) (IntVal b) = return $ IntVal $ a + b
evalBop Minus (IntVal a) (IntVal b) = return $ IntVal $ a - b
evalBop Times (IntVal a) (IntVal b) = return $ IntVal $ a * b
evalBop Divide (IntVal _) (IntVal 0) = throwError $ IntVal 1
evalBop Divide (IntVal a) (IntVal b) = return $ IntVal $ a `div` b
evalBop Gt (IntVal a) (IntVal b) = return $ BoolVal $ a > b
evalBop Ge (IntVal a) (IntVal b) = return $ BoolVal $ a >= b
evalBop Lt (IntVal a) (IntVal b) = return $ BoolVal $ a < b
evalBop Le (IntVal a) (IntVal b) = return $ BoolVal $ a <= b
evalBop _ _ _ = throwError $ IntVal 2

type EWS = ErrorT Value (WriterT String (StateT Store (Identity)))

execute :: Store -> Statement -> (Store, Maybe Value, String)
execute s p =
  case res of
    Left v -> (s', Just v, lg)
    Right _ -> (s', Nothing, lg)
  where ews = evalS p :: EWS ()
        ((res, lg), s') = runState (runWriterT (runErrorT ews)) s
  
raises :: Statement -> Value -> Test
s `raises` v = case (execute Map.empty s) of
    (_, Just v', _) -> v ~?= v'
    _  -> TestCase $ assertFailure "no Exception was raised"

t1 :: Test
t1 = (Assign "X"  (Var "Y")) `raises` IntVal 0

t2 :: Test
t2 = (Assign "X" (Op Divide (Val (IntVal 1)) (Val (IntVal 0)))) `raises` IntVal 1

t3 :: Test       
t3 = TestList [ Assign "X" (Op Plus (Val (IntVal 1)) (Val (BoolVal True))) `raises` IntVal 2,      
                If (Val (IntVal 1)) Skip Skip `raises` IntVal 2,
                While (Val (IntVal 1)) Skip `raises` IntVal 2]

mksequence :: [Statement] -> Statement
mksequence = foldr Sequence Skip

testprog1 :: Statement
testprog1 = mksequence [Assign "X" $ Val $ IntVal 0,
                        Assign "Y" $ Val $ IntVal 1,
                        Print "hello world: " $ Var "X",
                        If (Op Lt (Var "X") (Var "Y")) (Throw (Op Plus (Var "X") (Var "Y")))
                                                       Skip,
                        Assign "Z" $ Val $ IntVal 3]

t4 :: Test
t4 = execute Map.empty testprog1 ~?=
  (Map.fromList [("X", IntVal 0), ("Y",  IntVal 1)], Just (IntVal 1), "hello world: 0")

testprog2 :: Statement
testprog2 = mksequence [Assign "X" $ Val $ IntVal 0,
                        Assign "Y" $ Val $ IntVal 1,
                        Try (If (Op Lt (Var "X") (Var "Y"))
                                (mksequence [Assign "A" $ Val $ IntVal 100,
                                             Throw (Op Plus (Var "X") (Var "Y")),
                                             Assign "B" $ Val $ IntVal 200])
                                Skip)
                            "E"
                            (Assign "Z" $ Op Plus (Var "E") (Var "A"))]

t5 :: Test
t5 = execute Map.empty testprog2 ~?=
   ( Map.fromList [("A", IntVal 100), ("E", IntVal 1)
          ,("X", IntVal 0), ("Y", IntVal 1)
          ,("Z", IntVal 101)]
          , Nothing 
   , "")

main :: IO ()
main = do 
   _ <- runTestTT $ TestList [ t1, t2, t3, t4, t5 ]
   return ()

