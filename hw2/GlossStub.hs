{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}

-- A stubbed version of the Gloss libary 
-- Contains the basic data structures and operations, but doesn't display them
-- See http://gloss.ouroborus.net

module GlossStub where

import Data.Monoid

-- Instances -------------------------------------------------------------------------------------
instance Monoid Picture where
	mempty		= blank
	mappend a b	= Pictures [a, b]
	mconcat		= Pictures


-- | A point on the x-y plane.
--   Points can also be treated as `Vector`s, so "Graphics.Gloss.Data.Vector"
--   may also be useful.
type Point	= (Float, Float)			


-- | Pretend a point is a number.
--	Vectors aren't real numbes according to Haskell, because they don't
--	support the multiply and divide field operators. We can pretend they
--	are though, and use the (+) and (-) operators as component-wise
--	addition and subtraction.
--
instance Num Point where
	(+) (x1, y1) (x2, y2)	= (x1 + x2, y1 + y2)
 	(-) (x1, y1) (x2, y2)	= (x1 - x2, y1 - y2)
        (*) (x1, y1) (x2, y2)   = (x1 * x2, y1 * y2)
        signum (x, y)           = (signum x, signum y)
        abs    (x, y)           = (abs x, abs y)
	negate (x, y)		= (negate x, negate y)	
        fromInteger x           = (fromInteger x, fromInteger x)

-- | A path through the x-y plane.
type Path	= [Point]		


data Picture
	-- Primitives -------------------------------------

	-- | A blank picture, with nothing in it.
	= Blank

	-- | A convex polygon filled with a solid color.
	| Polygon 	Path
	
	-- | A line along an arbitrary path.
	| Line		Path

	-- | A circle with the given radius.
	| Circle	Float

	-- | A circle with the given thickness and radius.
	--   If the thickness is 0 then this is equivalent to `Circle`.
	| ThickCircle	Float Float

	-- | A circular arc drawn counter-clockwise between two angles 
        --  (in degrees) at the given radius.
        | Arc           Float Float Float

	-- | A circular arc drawn counter-clockwise between two angles 
        --  (in degrees), with the given radius  and thickness.
	--   If the thickness is 0 then this is equivalent to `Arc`.
        | ThickArc	Float Float Float Float

	-- | Some text to draw with a vector font.
	| Text		String

	-- Color ------------------------------------------
	-- | A picture drawn with this color.
	| Color		Color  		Picture

	-- Transforms -------------------------------------
	-- | A picture translated by the given x and y coordinates.
	| Translate	Float Float	Picture

	-- | A picture rotated clockwise by the given angle (in degrees).
	| Rotate	Float		Picture

	-- | A picture scaled by the given x and y factors.
	| Scale		Float	Float	Picture

	-- More Pictures ----------------------------------
	-- | A picture consisting of several others.
	| Pictures	[Picture]
	deriving (Show, Eq)


-- Instances ------------------------------------------------------------------


data Display = InWindow String (Int,Int) (Int, Int)
   | FullScreen (Int,Int)
  deriving (Eq, Read, Show)


-- Constructors ----------------------------------------------------------------
-- NOTE: The docs here should be identical to the ones on the constructors.

-- | A blank picture, with nothing in it.
blank :: Picture
blank	= Blank

-- | A convex polygon filled with a solid color.
polygon :: Path -> Picture
polygon = Polygon

-- | A line along an arbitrary path.
line :: Path -> Picture
line 	= Line

-- | A circle with the given radius.
circle  :: Float  -> Picture
circle 	= Circle

-- | A circle with the given thickness and radius.
--   If the thickness is 0 then this is equivalent to `Circle`.
thickCircle  :: Float -> Float -> Picture
thickCircle = ThickCircle

-- | A circular arc drawn counter-clockwise between two angles (in degrees) 
--   at the given radius.
arc     :: Float -> Float -> Float -> Picture
arc = Arc

-- | A circular arc drawn counter-clockwise between two angles (in degrees),
--   with the given radius  and thickness.
--   If the thickness is 0 then this is equivalent to `Arc`.
thickArc :: Float -> Float -> Float -> Float -> Picture
thickArc = ThickArc

-- | Some text to draw with a vector font.
text :: String -> Picture
text = Text

-- | A picture drawn with this color.
color :: Color -> Picture -> Picture
color = Color

-- | A picture translated by the given x and y coordinates.
translate :: Float -> Float -> Picture -> Picture
translate = Translate

-- | A picture rotated clockwise by the given angle (in degrees).
rotate  :: Float -> Picture -> Picture
rotate = Rotate

-- | A picture scaled by the given x and y factors.
scale   :: Float -> Float -> Picture -> Picture
scale = Scale

-- | A picture consisting of several others.
pictures :: [Picture] -> Picture
pictures = Pictures


-- Other Shapes ---------------------------------------------------------------

-- | A solid circle with the given radius.
circleSolid :: Float -> Picture
circleSolid r 
        = thickCircle (r/2) r


-- | A solid arc, drawn counter-clockwise between two angles at the given radius.
arcSolid  :: Float -> Float -> Float -> Picture
arcSolid a1 a2 r 
        = thickArc a1 a2 (r/2) r 



-- Rectangles -----------------------------------------------------------------
-- NOTE: Only the first of these rectangle functions has haddocks on the
--       arguments to reduce the amount of noise in the extracted docs.

-- | A path representing a rectangle centered about the origin
rectanglePath 
        :: Float        -- ^ width of rectangle
        -> Float        -- ^ height of rectangle
        -> Path
rectanglePath sizeX sizeY			
 = let	sx	= sizeX / 2
	sy	= sizeY / 2
   in	[(-sx, -sy), (-sx, sy), (sx, sy), (sx, -sy)]



-- | A path representing a rectangle in the y > 0 half of the x-y plane.
rectangleUpperPath :: Float -> Float -> Path
rectangleUpperPath sizeX sy
 = let 	sx	= sizeX / 2
   in  	[(-sx, 0), (-sx, sy), (sx, sy), (sx, 0)]


-- | A solid rectangle centered about the origin.
rectangleSolid :: Float -> Float -> Picture
rectangleSolid sizeX sizeY
	= Polygon $ rectanglePath sizeX sizeY


-- | A solid rectangle in the y > 0 half of the x-y plane.
rectangleUpperSolid :: Float -> Float -> Picture
rectangleUpperSolid sizeX sizeY
	= Polygon  $ rectangleUpperPath sizeX sizeY



-- | An abstract color value.
--	We keep the type abstract so we can be sure that the components
--	are in the required range. To make a custom color use 'makeColor'.
data Color
	-- | Holds the color components. All components lie in the range [0..1.
	= RGBA  !Float !Float !Float !Float
	deriving (Show, Eq)


instance Num Color where
 {-# INLINE (+) #-}
 (+) (RGBA r1 g1 b1 _) (RGBA r2 g2 b2 _)
        = RGBA (r1 + r2) (g1 + g2) (b1 + b2) 1

 {-# INLINE (-) #-}
 (-) (RGBA r1 g1 b1 _) (RGBA r2 g2 b2 _)
        = RGBA (r1 - r2) (g1 - g2) (b1 - b2) 1

 {-# INLINE (*) #-}
 (*) (RGBA r1 g1 b1 _) (RGBA r2 g2 b2 _)
        = RGBA (r1 * r2) (g1 * g2) (b1 * b2) 1

 {-# INLINE abs #-}
 abs (RGBA r1 g1 b1 _)
        = RGBA (abs r1) (abs g1) (abs b1) 1

 {-# INLINE signum #-}
 signum (RGBA r1 g1 b1 _)
        = RGBA (signum r1) (signum g1) (signum b1) 1
        
 {-# INLINE fromInteger #-}
 fromInteger i
  = let f = fromInteger i
    in  RGBA f f f 1


-- | Make a custom color. All components are clamped to the range  [0..1].
makeColor 
	:: Float 	-- ^ Red component.
	-> Float 	-- ^ Green component.
	-> Float 	-- ^ Blue component.
	-> Float 	-- ^ Alpha component.
	-> Color

makeColor r g b a
	= clampColor 
	$ RGBA r g b a
{-# INLINE makeColor #-}


-- | Make a custom color. 
--   You promise that all components are clamped to the range [0..1]
makeColor' :: Float -> Float -> Float -> Float -> Color
makeColor' r g b a
        = RGBA r g b a
{-# INLINE makeColor' #-}


-- | Make a custom color. All components are clamped to the range [0..255].
makeColor8 
	:: Int 		-- ^ Red component.
	-> Int 		-- ^ Green component.
	-> Int 		-- ^ Blue component.
	-> Int 		-- ^ Alpha component.
	-> Color

makeColor8 r g b a
	= clampColor 
	$ RGBA 	(fromIntegral r / 255) 
		(fromIntegral g / 255)
		(fromIntegral b / 255)
		(fromIntegral a / 255)
{-# INLINE makeColor8 #-}

	
-- | Take the RGBA components of a color.
rgbaOfColor :: Color -> (Float, Float, Float, Float)
rgbaOfColor (RGBA r g b a)	= (r, g, b, a)
{-# INLINE rgbaOfColor #-}
		

-- | Make a custom color.
--   Components should be in the range [0..1] but this is not checked.
rawColor
	:: Float	-- ^ Red component.
	-> Float	-- ^ Green component.
	-> Float 	-- ^ Blue component.
	-> Float 	-- ^ Alpha component.
	-> Color

rawColor = RGBA
{-# INLINE rawColor #-}


-- Internal 

-- | Clamp components of a color into the required range.
clampColor :: Color -> Color
clampColor cc
 = let	(r, g, b, a)	= rgbaOfColor cc
   in	RGBA (min 1 r) (min 1 g) (min 1 b) (min 1 a)

-- | Normalise a color to the value of its largest RGB component.
normaliseColor :: Color -> Color
normaliseColor cc
 = let	(r, g, b, a)	= rgbaOfColor cc
	m		= maximum [r, g, b]
   in	RGBA (r / m) (g / m) (b / m) a


-- Color functions ------------------------------------------------------------

-- | Mix two colors with the given ratios.
mixColors 
	:: Float 	-- ^ Ratio of first color.
	-> Float 	-- ^ Ratio of second color.
	-> Color 	-- ^ First color.
	-> Color 	-- ^ Second color.
	-> Color	-- ^ Resulting color.

mixColors ratio1 ratio2 c1 c2
 = let	RGBA r1 g1 b1 a1	= c1
	RGBA r2 g2 b2 a2	= c2

	total	= ratio1 + ratio2
	m1	= ratio1 / total
	m2	= ratio2 / total

   in	RGBA 	(m1 * r1 + m2 * r2)
		(m1 * g1 + m2 * g2)
		(m1 * b1 + m2 * b2)
		(m1 * a1 + m2 * a2)


-- | Add RGB components of a color component-wise, then normalise
--	them to the highest resulting one. The alpha components are averaged.
addColors :: Color -> Color -> Color
addColors c1 c2
 = let	RGBA r1 g1 b1 a1	= c1
	RGBA r2 g2 b2 a2	= c2

   in	normaliseColor 
	 $ RGBA (r1 + r2)
		(g1 + g2)
		(b1 + b2)
		((a1 + a2) / 2)


-- | Make a dimmer version of a color, scaling towards black.
dim :: Color -> Color
dim (RGBA r g b a)
	= RGBA (r / 1.2) (g / 1.2) (b / 1.2) a

	
-- | Make a brighter version of a color, scaling towards white.
bright :: Color -> Color
bright (RGBA r g b a)
	= clampColor
	$ RGBA (r * 1.2) (g * 1.2) (b * 1.2) a


-- | Lighten a color, adding white.
light :: Color -> Color
light (RGBA r g b a)
	= clampColor
	$ RGBA (r + 0.2) (g + 0.2) (b + 0.2) a
	
	
-- | Darken a color, adding black.
dark :: Color -> Color
dark (RGBA r g b a)
	= clampColor
	$ RGBA (r - 0.2) (g - 0.2) (b - 0.2) a


-- Pre-defined Colors ---------------------------------------------------------
-- | A greyness of a given magnitude.
greyN 	:: Float 	-- ^ Range is 0 = black, to 1 = white.
	-> Color
greyN n		= RGBA n   n   n   1.0

black, white :: Color
black		= RGBA 0.0 0.0 0.0 1.0
white		= RGBA 1.0 1.0 1.0 1.0

-- Colors from the additive color wheel.
red, green, blue :: Color
red		= RGBA 1.0 0.0 0.0 1.0
green		= RGBA 0.0 1.0 0.0 1.0
blue		= RGBA 0.0 0.0 1.0 1.0

-- secondary
yellow, cyan, magenta :: Color
yellow		= addColors red   green
cyan		= addColors green blue
magenta		= addColors red   blue

-- tertiary
rose, violet, azure, aquamarine, chartreuse, orange :: Color
rose		= addColors red     magenta
violet		= addColors magenta blue
azure		= addColors blue    cyan
aquamarine	= addColors cyan    green
chartreuse	= addColors green   yellow
orange		= addColors yellow  red


display :: Display          -- ^ Display mode.
        -> Color            -- ^ Background color.
        -> Picture          -- ^ The picture to draw.
        -> IO ()

display = error "Cannot actually display pictures"