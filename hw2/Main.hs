-- Advanced Programming, HW 2
-- by Raul Martinez, mraul
-- and Honk Kim, honki

{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Main where
import Prelude hiding (takeWhile,all)
import Test.HUnit      -- unit test support
-- import Graphics.Gloss hiding (play) -- graphics library for problem 2
import GlossStub    -- "stubbed" version of gloss if you have trouble
import XMLTypes        -- support file for problem 3 (provided)
import Play            -- support file for problem 3 (provided)

doTests :: IO ()
doTests = do
  _ <- runTestTT $ TestList [ test0, test1, test2, test3 ]
  return ()

main :: IO ()
main = do
       doTests
       -- drawCircles   --- graphics demo, change to drawTree for your HW
       -- drawSierpinski
       drawTree
       return ()

--------------------------------------------------------------------------------

-- 2 (a)

-- | The intersperse function takes an element and a list
-- and `intersperses' that element between the elements of the list.
-- For example,
--    intersperse ',' "abcde" == "a,b,c,d,e"

intersperse :: a -> [a] -> [a]
intersperse e = foldr combine [] where
    combine a [] = [a]
    combine a b = a:e:b
t2a :: Test
t2a = "2a" ~:
      TestList [ intersperse ',' "abcde" ~?= "a,b,c,d,e",
                 intersperse ',' []      ~?= [],
                 intersperse 1 [1]       ~?= [1] ]


-- 2 (b)

-- | invert lst returns a list with each pair reversed.
-- for example:
--   invert [("a",1),("a",2)] returns [(1,"a"),(2,"a")]

invert :: [(a, b)] -> [(b, a)]
invert = map (\(x,y) -> (y,x))
t2b :: Test
t2b = "2b" ~:
      TestList [ invert [("a",1),("a",2)]  ~?= [(1,"a"),(2,"a")],
                 invert [(1, 2),(3, 4)]    ~?= [(2, 1),(4, 3)],
                 invert ([]::[(Int, Int)]) ~?= [] ]


-- 2 (c)

-- | takeWhile, applied to a predicate p and a list xs,
-- returns the longest prefix (possibly empty) of xs of elements
-- that satisfy p:
-- For example,
--     takeWhile (< 3) [1,2,3,4,1,2,3,4] == [1,2]
--     takeWhile (< 9) [1,2,3] == [1,2,3]
--     takeWhile (< 0) [1,2,3] == []

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile p = foldr (\a b -> if p a then a:b else []) []
t2c :: Test
t2c = "2c" ~:
      TestList [ takeWhile (< 3) [1,2,3,4,1,2,3,4] ~?= [1,2],
                 takeWhile (< 9) [1,2,3]           ~?= [1,2,3],
                 takeWhile (< 0) [1,2,3]           ~?= [],
                 takeWhile (even) [0,2,4,3,4,6]    ~?= [0,2,4] ]


-- 2 (d)

-- | find pred lst returns the first element of the list that
-- satisfies the predicate. Because no element may do so, the
-- answer is returned in a "Maybe".
-- for example:
--     find odd [0,2,3,4] returns Just 3

find :: (a -> Bool) -> [a] -> Maybe a
find p = foldr (\a b -> if p a then Just a else b) Nothing
t2d :: Test
t2d = "2d" ~: TestList [ find odd [0,2,3,4]  ~?= Just 3,
                         find even [0,2,3,4] ~?= Just 0,
                         find odd []         ~?= Nothing ]


-- 2 (e)

-- | all pred lst returns False if any element of lst
-- fails to satisfy pred and True otherwise.
-- for example:
--    all odd [1,2,3] returns False

all :: (a -> Bool) -> [a] -> Bool
all p = foldr (\a b -> p a && b) True
t2e :: Test
t2e = "2e" ~: TestList [ all odd [1,2,3]   ~?= False,
                         all even [1,2,3]  ~?= False,
                         all (< 5) [1,2,3] ~?= True,
                         all even []       ~?= True ]


test2 :: Test
test2 = TestList [t2a, t2b, t2c, t2d, t2e]

--------------------------------------------------------------------------------

-- | a basic tree data structure
data Tree a = Leaf | Branch a (Tree a) (Tree a) deriving (Show, Eq)

foldTree :: b -> (a -> b -> b -> b) -> Tree a -> b
foldTree e _ Leaf     = e
foldTree e n (Branch a n1 n2) = n a (foldTree e n n1) (foldTree e n n2)

mapTree :: (a -> b) -> Tree a -> Tree b
mapTree f = foldTree Leaf (\x t1 t2 -> Branch (f x) t1 t2)


-- 0 (a)

-- | The invertTree function takes a tree of pairs and returns a new tree
-- with each pair reversed.  For example:
--     invertTree (Branch ("a",1) Leaf Leaf) returns Branch (1,"a") Leaf Leaf

invertTree :: Tree (a,b) -> Tree (b,a)
invertTree = mapTree (\(x,y) -> (y,x))
t0a :: Test
t0a = "0a" ~:
      TestList [
       invertTree (Branch ("a",1) Leaf Leaf) ~?= Branch (1,"a") Leaf Leaf,
       invertTree (Branch ("a",1) (Branch ("b",2) Leaf Leaf) Leaf) ~?=
                  (Branch (1,"a") (Branch (2, "b") Leaf Leaf) Leaf) ]


-- 0 (b)

-- | takeWhileTree, applied to a predicate p and a tree t,
-- returns the largest prefix tree of t  (possibly empty)
-- where all elements satisfy p.
-- For example, given the following tree

tree1 :: Tree Int
tree1 = Branch 1 (Branch 2 Leaf Leaf) (Branch 3 Leaf Leaf)

--     takeWhileTree (< 3) tree1  returns Branch 1 (Branch 2 Leaf Leaf) Leaf
--     takeWhileTree (< 9) tree1  returns tree1
--     takeWhileTree (< 0) tree1  returns Leaf

takeWhileTree :: (a -> Bool) -> Tree a -> Tree a
takeWhileTree p = foldTree Leaf (\a t1 t2 -> if p a then Branch a t1 t2 else Leaf)
t0b :: Test
t0b = "0b" ~:
      TestList [ takeWhileTree (< 3) tree1
                                   ~?= Branch 1 (Branch 2 Leaf Leaf) Leaf,
                 takeWhileTree (< 9) tree1  ~?= tree1,
                 takeWhileTree (< 0) tree1  ~?= Leaf ]


-- 0 (c)

-- | allTree pred tree returns False if any element of tree
-- fails to satisfy pred and True otherwise.
-- for example:
--    allTree odd tree1 returns False

tree2 :: Tree Int
tree2 = Branch 1 (Branch 5 Leaf Leaf) (Branch 3 Leaf Leaf)

allTree :: (a -> Bool) -> Tree a -> Bool
allTree p = foldTree True (\a b1 b2 -> p a && b1 && b2)
t0c :: Test
t0c = "0c" ~:
      TestList [ allTree odd tree1 ~?= False,
                 allTree odd tree2 ~?= True ]


-- | 0 (d) WARNING: This one is a bit tricky!  (Hint: the value
-- *returned* by foldTree can itself be a function.)

-- | map2Tree f xs ys returns the tree obtained by applying f to
-- to each pair of corresponding elements of xs and ys. If
-- one branch is longer than the other, then the extra elements
-- are ignored.
-- for example:
--    map2Tree (+) (Branch 1 Leaf (Branch 2 Leaf Leaf)) (Branch 3 Leaf Leaf)
--        should return (Branch 4 Leaf Leaf)

map2Tree :: (a -> b -> c) -> Tree a -> Tree b -> Tree c
map2Tree f t1 t2 = transform t2
  where transform = foldTree (const Leaf) combine t1
        combine _ _ _ Leaf                 = Leaf
        combine a f1 f2 (Branch b t1' t2') = Branch (f a b) (f1 t1') (f2 t2')

t0d :: Test
t0d = "0d" ~:
      TestList [
       map2Tree (+) (Branch 1 Leaf (Branch 2 Leaf Leaf)) (Branch 3 Leaf Leaf)
                    ~?= (Branch 4 Leaf Leaf) ]

-- 0 (e)

-- | zipTree takes two trees and returns a tree of corresponding pairs. If
-- one input branch is smaller, excess elements of the longer branch are
-- discarded.
-- for example:
--    zipTree (Branch 1 (Branch 2 Leaf Leaf) Leaf) (Branch True Leaf Leaf) returns
--            (Branch (1,True) Leaf Leaf)
--
-- To use foldTree, you'll need to think about this one in
-- the same way as part (d).

zipTree :: Tree a -> Tree b -> Tree (a,b)
zipTree = map2Tree (,)

t0e :: Test
t0e = "0e" ~: zipTree (Branch 1 (Branch 2 Leaf Leaf) Leaf) (Branch True Leaf Leaf)
                       ~?= (Branch (1,True) Leaf Leaf)

test0 :: Test
test0 = TestList [ t0a, t0b, t0c, t0d, t0e ]

--------------------------------------------------------------------------------

-- | Display a gloss picture, given the name of the window, size of the
-- window, position, background color and the picture itself.
displayInWindow :: String -> (Int, Int) -> (Int, Int) -> Color -> Picture -> IO ()
displayInWindow x y z = display (InWindow x y z)

-- | a picture composed of concentric circles
circles :: Picture
circles = pictures (reverse colorCircles) where
   -- a list of circle pictures with increasing radii
   bwCircles :: [Picture]
   bwCircles = map (\f -> circleSolid (25.0 * f)) [1.0 ..]
   -- a list of builtin colors
   colors :: [Color]
   colors    = [red, blue, green, cyan, magenta, yellow]
   -- a list of colored circles
   colorCircles :: [Picture]
   colorCircles = zipWith color colors bwCircles

-- | draw the concentric circle picture in a window
-- this variable is an "action" of type IO (). Running the action
-- in the main program will open a window (of size (600,600)) and
-- display the circles.

drawCircles :: IO ()
drawCircles = displayInWindow "Circles" (600,600) (10,10) black circles

-- | a right triangle at position `x` `y` with side length `size`
triangle :: Float -> Float -> Float -> Picture
triangle x y size = line [(x,y), (x+size, y), (x, y-size), (x,y)]

minSize :: Float
minSize = 8.0

-- | a sierpinski triangle
sierpinski :: Float -> Float -> Float -> [ Picture ]
sierpinski x y size =
  if size <= minSize
  then [ triangle x y size ]
  else let size2 = size / 2
       in sierpinski x y size2 ++
          sierpinski x (y-size2) size2 ++
          sierpinski (x+size2) y size2

-- | the action to draw the triangle on the screen
drawSierpinski :: IO ()
drawSierpinski =
   displayInWindow "Sierpinski" (600,600) (10,10) white sierpinskiPicture where
      sierpinskiPicture = color blue (pictures (sierpinski 0 0 256))


-- 1 (a)

calcSize :: Float -> Float -> Tree Float
calcSize ratio size
    | size <= minSize = Leaf
    | otherwise       = Branch size tree tree
    where tree  = calcSize ratio size2
          size2 = size * ratio;

t1a :: Test
t1a = "1a" ~: calcSize 0.5 25 ~=?
         Branch 25.0 (Branch 12.5 Leaf Leaf) (Branch 12.5 Leaf Leaf)


-- 1 (b)

fractal :: Float -> Float -> Float -> Float -> Tree Float -> Tree Picture
fractal _ _ _ _ Leaf = Leaf
fractal delta angle x y (Branch size lt rt) = Branch pic lt' rt'
    where pic = Line [(x, y), (x', y')]
          x' = x + size * cos angle
          y' = y + size * sin angle
          lt' = fractal delta (angle + delta) x' y' lt
          rt' = fractal delta (angle - delta) x' y' rt

t1b :: Test
t1b = "1b" ~: fractal (pi/2) 0 0 0 (calcSize 0.5 25) ~=?
               Branch (Line [(0.0,0.0),(25.0,0.0)])
                  (Branch (Line [(25.0,0.0),(25.0,12.5)]) Leaf Leaf)
                  (Branch (Line [(25.0,0.0),(25.0,-12.5)]) Leaf Leaf)


-- 1 (c)

join :: Tree Picture -> Picture
join Leaf                = Blank
join (Branch pic lt rt) = Pictures (pic:(join lt):[join rt])

t1c :: Test
t1c = "1c" ~: join (Branch Blank Leaf Leaf) ~?= Pictures [Blank, Blank, Blank]

-- | create a fractal tree with some initial parameters. Try changing
-- some of these values to see how that changes the tree. In particular,
-- try changing the delta for the angle of the branches (initially pi/6 below).
fractalTree :: Picture
fractalTree = color blue (join (fractal (pi/6) (pi/2) 0 0 sizeTree)) where
   sizeTree = calcSize 0.6 150.0

drawTree :: IO ()
drawTree = displayInWindow "MyWindow" (700,700) (10,10) white fractalTree

test1 :: Test
test1 = TestList [t1a,t1b,t1c]

--------------------------------------------------------------------------------

-- 3

-- | GENERIC HELPER FUNCTIONS
-- These functions are generic functions operating on XML Elements. These include
-- changing the tag of an element, adding a parent elemement, appending to a
-- sibling element, flattening of elements, wrapping element in bold tag, adding
-- line break tag and formatting an element. Formatting an element requires the
-- user to define a transform element function.

changeTag :: SimpleXML -> ElementName -> SimpleXML
changeTag (Element _ l) newName = Element newName l
changeTag pd _ = pd

addParent :: ElementName -> [SimpleXML] -> SimpleXML
addParent elName children = Element elName children

appendSibling :: ElementName -> [SimpleXML] -> [SimpleXML]
appendSibling elName siblings = siblings ++ [Element elName []]

flattenChildren :: (SimpleXML -> [SimpleXML]) -> [SimpleXML] -> [SimpleXML]
flattenChildren f l = concatMap f l

flattenContainer :: ElementName -> [SimpleXML] -> [SimpleXML] -> [SimpleXML]
flattenContainer elName cl l
    | elem elName containerTags = l ++ flattenChildren (formatElem elName) cl
    | otherwise = l

makeBold :: ElementName -> [SimpleXML] -> [SimpleXML]
makeBold elName l
    | elem elName boldTags = [addParent "b" l]
    | otherwise = l

addNewLine :: ElementName -> [SimpleXML] -> [SimpleXML]
addNewLine elName l
    | elem elName newLineTags = appendSibling "br" l
    | otherwise = l

formatElem :: ElementName -> SimpleXML -> [SimpleXML]
formatElem parent (Element elName l) =
    let fList = (flattenContainer elName l) :
                map (\f -> (f elName)) [addNewLine, makeBold] in
    foldr (\f' l' -> f' l') (transformElem parent elName l) fList
formatElem _ x = [x]

--------------------------------------------------------------------------------
-- | "PLAY" SPECIFIC FUNCTIONS
-- These functions are specific to transforming plays. The user must specify
-- at least transformElem. The user could also define lists containing tags
-- specific to the document being transformed and used in deciding what to
-- do in transformElem.

formatPlay :: SimpleXML -> SimpleXML
formatPlay el = addParent "html" [addParent "body" (formatElem "ROOT" el)]

containerTags :: [ElementName]
containerTags = ["PLAY", "ACT", "SCENE", "SPEECH", "PERSONAE"]

boldTags :: [ElementName]
boldTags = ["SPEAKER"]

newLineTags :: [ElementName]
newLineTags = ["PERSONA", "SPEAKER", "LINE"]

transformElem :: ElementName -> ElementName -> [SimpleXML] -> [SimpleXML]
transformElem parent elName l
    | elName == "TITLE" =
        case parent of
          "PLAY"  -> [Element "h1" l]
          "ACT"   -> [Element "h2" l]
          "SCENE" -> [Element "h3" l]
          _ -> error "title transform not defined for given parent"
    | elName == "PERSONAE" = [Element "h2" [PCDATA "Dramatis Personae"]]
    | elem elName containerTags = []
    | elem elName newLineTags = l
    | otherwise = error "tag transform not defined"

--------------------------------------------------------------------------------

firstDiff :: Eq a => [a] -> [a] -> Maybe ([a],[a])
firstDiff [] [] = Nothing
firstDiff (c:cs) (d:ds)
    | c==d = firstDiff cs ds
    | otherwise = Just (c:cs, d:ds)
firstDiff cs ds = Just (cs,ds)

-- | Test the two files character by character, to determine whether
-- they match.
testResults :: String -> String -> IO ()
testResults file1 file2 = do
  f1 <- readFile file1
  f2 <- readFile file2
  case firstDiff f1 f2 of
    Nothing -> return ()
    Just (cs,ds) -> assertFailure msg where
      msg  = "Results differ: '" ++ take 20 cs ++
            "' vs '" ++ take 20 ds

test3 :: Test
test3 = TestCase $ do
  writeFile "dream.html" (xml2string (formatPlay play))
  testResults "dream.html" "sample.html"
