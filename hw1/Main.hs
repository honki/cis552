-- Advanced Programming, HW 1
-- by Dong Lee (Justin), donghlee
-- and Hong Kim, honki

{-# OPTIONS -Wall -fwarn-tabs -fno-warn-type-defaults  #-}

{-# LANGUAGE NoImplicitPrelude #-}

module Main where
import Prelude hiding (all, reverse, takeWhile, zip)
import Test.HUnit 
import Control.Applicative ((<*>))

main :: IO ()
main = do 
   _ <- runTestTT $ TestList [ test0, test1, test2, test3, test4 ]
   return ()

-- Part 0 (a)

booleanOp :: Bool -> Bool -> Bool -> Bool
booleanOp x y z = x && (y || z)
 
t0a :: Test
t0a = "0a1" ~: TestList [booleanOp True False True ~?= True, 
                         booleanOp True False False ~?= False,
                         booleanOp False True True ~?= False]

-- 0 (b)

arithmetic :: ((Int, Int), Int) -> ((Int,Int), Int) -> (Int, Int, Int)
arithmetic ((a,b), c) ((d, e), f) = (b*f - c*e, c*d - a*f, a*e - b*d) 

t0b :: Test
t0b = "0b" ~: TestList[ arithmetic ((1,2),3) ((4,5),6) ~?= (-3,6,-3), 
                        arithmetic ((3,2),1) ((4,5),6) ~?= (7,-14,7) ]

-- 0 (c)

cmax :: [Int] -> Int -> Int
cmax xs t   = foldr max t xs
                
t0c :: Test
t0c ="0c" ~: TestList[ cmax [1,4,2] 0 ~?= 4, 
                       cmax []      0 ~?= 0,
                       cmax [5,1,5] 0 ~?= 5 ]

-- 0 (d)

reverse :: [a] -> [a]
reverse []      = []
reverse (x:xs)  = reverse xs ++ [x]

t0d :: Test
t0d = "0d" ~: TestList [reverse [3,2,1] ~?= [1,2,3],
                        reverse [1]     ~?= [1] ]

test0 :: Test
test0 = "test0" ~: TestList [ t0a , t0b, t0c, t0d ]


-- Problem 1

bowlingTest0 :: ([ Int ] -> Int) -> Test
bowlingTest0 score = "gutter ball" ~: 0 ~=? score (replicate 20 0)

score0 :: [ Int ] -> Int
score0 _ = 0

bowlingTest1 :: ([ Int ] -> Int) -> Test
bowlingTest1 score = 
   "allOnes" ~: 20 ~=? score (replicate 20 1)

score1 :: [ Int ] -> Int
score1 []       = 0
score1 (x:xs)   = x + score1 xs

bowlingTest2 :: ([ Int ] -> Int) -> Test
bowlingTest2 score = "spare game" ~: 16 ~=? score ([5,5,3] ++ replicate 17 0)

score2 :: [ Int ] -> Int
score2 []     = 0
score2 (x1:x2:x3:xs)
  | x1 + x2 == 10 = x1 + x2 + x3 + (score2 (x3:xs))
score2 (x:xs) = x + (score2 xs)

score2a :: [ Int ] -> Int
score2a []          = 0
score2a (r1:r2:r3:rs)
  | r1 + r2 == 10   = r1 + r2 + r3 + (score2a (r3:rs))
score2a (r:rs)      = r + (score2a rs)

bowlingTest3 :: ([ Int ] -> Int) -> Test
bowlingTest3 score = "strike game" ~: 28 ~=? score ([10, 5, 4] ++ replicate 16 0)

score3 :: [ Int ] -> Int
score3 []           = 0
score3 (r1:r2:r3:rs)
  | r1 + r2 == 10   = r1 + r2 + r3 + (score3 (r3:rs))
  | r1 == 10        = r1 + r2 + r3 + (score3 (r2:r3:rs))
score3 (r:rs)       = r + (score3 rs)


bowlingTest4 :: ([ Int ] -> Int) -> Test
bowlingTest4 score = "perfect game" ~: 300 ~=? score (replicate 12 10) 

score4 :: [ Int ] -> Int
score4 l = score4_aux l 1 where
  score4_aux (r1:r2:r3:rs) fr
    | fr < 10 && r1 + r2 == 10 = r1 + r2 + r3 + (score4_aux (r3:rs) (fr+1))
    | fr < 10 && r1 == 10      = r1 + r2 + r3 + (score4_aux (r2:r3:rs) (fr+1))
  score4_aux (r1:r2:rs) fr     = r1 + r2 + (score4_aux rs (fr+1))
  score4_aux [r] _             = r 
  score4_aux [] _              = 0


test1 :: Test
test1 = TestList (map checkOutput scores) where
  -- the five test cases, in order 
  bowlingTests  = [bowlingTest0, bowlingTest1, bowlingTest2, 
                   bowlingTest3, bowlingTest4]
 
  -- the names of the score functions, the functions themselves, 
  -- and the expected number of passing tests
  scores = zip3 ['0' ..] [score0, score1, score2a, score3, score4] [1..]
 
  -- a way to run a unit test without printing output 
  testSilently = performTest (\ _ _ -> return ()) 
                   (\ _ _ _ -> return ()) (\ _ _ _ -> return ()) ()
 
  -- run each bowling test on the given score function, making sure that 
  -- the expected number of tests pass.
  checkOutput (name, score, pass) = " Testing score" ++ [name] ~: do 
    (s0,_) <- testSilently (TestList $ bowlingTests <*> [score])
    assert $ pass @=? cases s0 - (errors s0 + failures s0)

-- Problem 2

-- 2a)

-- | The conv function takes two lists of numbers, reverses the 
-- second list, multiplies their elements together pointwise, and sums
-- the result.  This function assumes that the two input
-- lists are the same length.
 
conv :: [Int] -> [Int] -> Int
conv l1 l2 = conv_aux l1 (reverse l2) where
  conv_aux [] []          = 0
  conv_aux (x:xs) (y:ys)  = x*y + conv_aux xs ys
  conv_aux _ _            = error "Lists are not of same length"

t2a :: Test
t2a = "2a" ~: conv [2,4,6] [1,2,3] ~?= 20

-- 2b) 

-- | The normalize function adds extra zeros to the beginning of each
-- list so that they each have exactly length 2n, where n is 
-- the length of the longer number.   
 
normalize :: [Int] -> [Int] -> ([Int], [Int])
normalize l1 l2 = (padList (2*n) l1, padList (2*n) l2) where   
  n = max (length l1) (length l2)
  padList len l = (replicate (len - length l) 0) ++ l


t2b :: Test
t2b = "2b" ~: normalize [1] [2,3] ~?= ([0,0,0,1], [0,0,2,3])

-- 2c)

-- | multiply two numbers, expressed as lists of digits, using 
-- the Åªrdhva TiryagbhyÄm algorithm.
 
multiply :: [Int] -> [Int] -> [Int]
multiply l1 l2 = fst (mult_aux (normalize l1 l2)) where
  mult_aux ([], []) = ([], 0)
  mult_aux ((x:xs), (y:ys)) = 
    let (tl, c)  = mult_aux (xs, ys) 
        s        = conv (x:xs) (y:ys)
        z        = (s+c) `mod` 10
        c'       = (s+c) `div` 10
    in (z:tl, c')
  mult_aux (_, _)  = error "Lists of different length."



t2c :: Test
t2c = "2c" ~: multiply [9,4,6][9,2,3] ~?= [8,7,3,1,5,8]

-- 2d) (Warning, this one may be tricky!)

convAlt :: [Int] -> [Int] -> Int
convAlt l1 l2 = fst (convAlt_aux l1 l2) where
  convAlt_aux [] l2' = (0, l2')
  convAlt_aux (x:xs) l2' =
    let (result, ys) = convAlt_aux xs l2'
    in (x*(head ys) + result, (tail ys))


t2d :: Test
t2d = "2d" ~: convAlt [2,4,6][1,2,3] ~=? 20

test2 :: Test
test2 = TestList [t2a,t2b,t2c,t2d]

-- Problem 3

test3 :: Test
test3 = "test3" ~: TestList [t3a, t3b, t3c, t3d, t3e, t3f, t3g, t3h]

-- 3 (a)

-- The intersperse function takes an element and a list 
-- and `intersperses' that element between the elements of the list. 
-- For example,
--    intersperse ',' "abcde" == "a,b,c,d,e"

intersperse :: a -> [a] -> [a]
intersperse e (x1:x2:xs) = x1:e:(intersperse e (x2:xs))
intersperse _ l = l
 
t3a :: Test
t3a = "3a" ~: intersperse ',' "abcde" ~?= "a,b,c,d,e"


-- 3 (b)

-- invert lst returns a list with each pair reversed. 
-- for example:
--   invert [("a",1),("a",2)]     returns [(1,"a"),(2,"a")] 
--   invert ([] :: [(Int,Char)])  returns []

--   note, you need to add a type annotation to test invert with []
--    

invert :: [(a, b)] -> [(b, a)]
invert []          = []
invert ((x, y):tl) = (y, x) : invert tl
 
t3b :: Test
t3b = "3b" ~: invert [("a", 1), ("a", 2)] ~?= [(1, "a"), (2, "a")]
 

-- 3 (c)

-- takeWhile, applied to a predicate p and a list xs, 
-- returns the longest prefix (possibly empty) of xs of elements 
-- that satisfy p:
-- For example, 
--     takeWhile (< 3) [1,2,3,4,1,2,3,4] == [1,2]
--     takeWhile (< 9) [1,2,3] == [1,2,3]
--     takeWhile (< 0) [1,2,3] == []

takeWhile :: (a -> Bool) -> [a] -> [a]
takeWhile _ []      = []
takeWhile p (x:xs)  = if p x then x : takeWhile p xs else []
 
t3c :: Test
t3c = "3c" ~: takeWhile (< 4) [1, 2, 3, 4] ~?= [1,2,3]
 

-- 3 (d)

-- find pred lst returns the first element of the list that 
-- satisfies the predicate. Because no element may do so, the 
-- answer is returned in a "Maybe".
-- for example: 
--     find odd [0,2,3,4] returns Just 3

find :: (a -> Bool) -> [a] -> Maybe a
find _ []     = Nothing
find p (x:xs) = if p x then Just x else find p xs
 
t3d :: Test
t3d = "3d" ~: TestList [ find odd [0,2,3,4] ~?= Just 3,
                         find odd [0,2,4] ~?= Nothing ]
 

-- 3 (e)

-- all pred lst returns False if any element of lst fails to satisfy
-- pred and True otherwise.
-- for example:
--    all odd [1,2,3] returns False

all :: (a -> Bool) -> [a] -> Bool
all _ []     = True
all p (x:xs) = p x && (all p xs)
 
t3e :: Test
t3e = "3e" ~: TestList [all odd [1, 2, 3] ~?= False,
                        all odd [1,1,3] ~?= True ]
 

-- 3 (f)

-- map2 f xs ys returns the list obtained by applying f to 
-- to each pair of corresponding elements of xs and ys. If 
-- one list is longer than the other, then the extra elements 
-- are ignored.
-- i.e. 
--   map2 f [x1, x2, ..., xn] [y1, y2, ..., yn, yn+1] 
--        returns [f x1 y1, f x2 y2, ..., f xn yn]

map2 :: (a -> b -> c) -> [a] -> [b] -> [c]
map2 f (x:xs) (y:ys) = f x y : map2 f xs ys 
map2 _ _ _           = []

t3f :: Test
t3f = "3f" ~: map2 (+) [1, 1, 1] [3,3,3] ~?= [4,4,4] 

-- 3 (g)

-- zip takes two lists and returns a list of corresponding pairs. If
-- one input list is shorter, excess elements of the longer list are
-- discarded.
-- for example:  
--    zip [1,2] [True] returns [(1,True)]

zip :: [a] -> [b] -> [(a,b)]
zip (x:xs) (y:ys) = (x,y) : zip xs ys
zip _ _           = []

t3g :: Test
t3g = "3g" ~: zip [1,2] [True] ~?= [(1, True)]

-- 3 (h)  (WARNING: this one is tricky!)

-- The transpose function transposes the rows and columns of its argument. 
-- If the inner lists are not all the same length, then the extra elements
-- are ignored.
-- for example:
--    transpose [[1,2,3],[4,5,6]] returns [[1,4],[2,5],[3,6]]

transpose :: [[a]] -> [[a]]
transpose l =
  if all (not . null) l
    then map head l : transpose (map tail l)
    else []

t3h :: Test
t3h = "3h" ~: transpose [[1,2,3,4], [4,5,6]] ~?= [[1,4], [2,5], [3,6]]

-- Problem 4

lcs :: String -> String -> String 
lcs = error "unimplemented: lcs"

test4 :: Test
test4 = "4" ~: TestList [ lcs "Advanced" "Advantaged" ~?= "Advaned" ]

test4a :: Test
test4a = "4a" ~: lcs "abcd" "acbd" ~?= "acd"

test4b :: Test
test4b = "4b" ~: lcs "abcd" "acbd" ~?= "abd"


